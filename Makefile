build:
	docker build -t ${APP_IMAGE} .
	docker build -t ${NGINX_IMAGE} nginx
up:
	make docker_create docker_migrate docker_start
down:
	docker-compose down
docker_create:
	docker-compose up --no-start
docker_start:
	docker-compose start
docker_stop:
	docker-compose stop
pull:
	docker pull ${APP_IMAGE}
	docker pull ${NGINX_IMAGE}
	docker-compose pull db
push:
	docker push ${APP_IMAGE}
	docker push ${NGINX_IMAGE}
migrate:
	python src/manage.py migrate $(if $m, api $m,)
docker_migrate:
	docker-compose up -d db
	docker-compose run --rm web make migrate
	docker-compose stop db
test:
	pytest
docker_test:
	docker-compose up -d db
	docker-compose run --rm web make test
	docker-compose stop db
make_migrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

create_superuser:
	python src/manage.py createsuperuser

collect_static:
	python src/manage.py collectstatic --no-input

copy_env_example:
	cp .env.example src/.env

run_server:
	python src/manage.py runserver 0.0.0.0:8000

run_bot:
	python src/manage.py runbot

run_prometheus_server:
	python src/manage.py runprometheusserver

dev:
	make run_bot & make run_server

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

lock:
	poetry lock
	sudo chown -R ${USER} poetry.lock

poetry_shell:
	poetry shell

lint:
	isort src
	black --config pyproject.toml src
check_lint:
	isort --check --diff src
	black --config pyproject.toml --check src
	flake8 --config pyproject.toml src
docker_check_lint:
	docker container run --rm --name dt-backend-lint ${APP_IMAGE} make check_lint

wait:
	./docker-compose-wait
