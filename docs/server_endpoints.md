# Server endpoints
## API
You can get all the API endpoints in /api/docs after running a server.
## Admin site
You can open the Django admin site in /admin. In order to log in you have to create a superuser:
```shell
make create_superuser
```