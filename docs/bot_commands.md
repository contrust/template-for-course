# Telegram bot commands
| Command | Description                                                                                |
| --- |--------------------------------------------------------------------------------------------|
| /help | Show the help message                                                                      |
| /start | Create or update your data in the database                                    |
| /set_phone | Set the phone number in order to do other commands                                         |
| /me | Show all your information                                                                  |                 |
| /bank_account_list | Show all the bank accounts                                                                 |                        
| /bank_account_balance | Show the balance on given bank account                                                     |             
| /bank_account_cards | Show all the cards attached to given bank account                                          |
| /bank_card_balance | Show the balance on given bank card                                                        |
| /favorites | Show all the favorite users                                                                |
| /add_favorite | Add a user to the list of favorites                                                        |
| /remove_favorite | Remove a user from the list of favorites                                                   |
| /send_money_to_card | Send money to the specified bank card                                                      |
| /send_money_to_account | Send money to the specified bank account                                                   |
| /send_money_to_user | Send money to the specified user                                                           |
| /interacted_users | Show all users who you sent money to or received from ('-' means the user has no name) |
| /bank_account_statement | Show your bank account statement |
| /bank_card_statement | Show the bank card's account statement |
| /unread_money_transactions | Show all the unread received money transactions |
| /set_password | Set a password for logging in the app |
| /login | Get new access and refresh tokens for logging in the app |
| /cancel | Cancel the current command |