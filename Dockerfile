FROM python:3.10.6-slim-buster

WORKDIR /app

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait docker-compose-wait
RUN chmod +x docker-compose-wait

RUN apt update && \
    apt install make && \
    rm -rf /var/lib/apt/lists/*

COPY pyproject.toml poetry.lock ./

RUN pip install --no-cache-dir poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-root && \
    rm -rf poetry.lock

COPY Makefile setup.cfg ./

COPY ./src ./src