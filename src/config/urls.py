from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.template.defaulttags import url
from django.urls import include, path

from app.internal.api import ninja_api

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", ninja_api.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
