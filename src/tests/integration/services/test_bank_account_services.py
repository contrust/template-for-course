import pytest

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.exceptions.db.repositories import (
    BankAccountWithNumberNotFoundException,
)
from tests.conftest import bank_account_service


@pytest.mark.django_db
def test_get_bank_account_when_there_is_no_account(create_bot_user1):
    with pytest.raises(BankAccountWithNumberNotFoundException):
        bank_account_service.get_bank_account_by_number("1" * 20)


@pytest.mark.django_db
def test_get_bank_account_when_there_is_account(create_bot_user1_with_bank_account1):
    assert bank_account_service.get_bank_account_by_number("1" * 20) is not None


@pytest.mark.django_db
def test_get_all_bank_accounts_when_there_are_no_accounts(create_bot_user1):
    assert bank_account_service.get_all_bank_accounts_numbers_of_user(1) == []


@pytest.mark.django_db
def test_get_all_bank_accounts_when_there_is_account(create_bot_user1_with_bank_account1):
    assert bank_account_service.get_all_bank_accounts_numbers_of_user(1) == ["1" * 20]


@pytest.mark.django_db
def test_get_bank_account_balance_raise_exception_when_no_account():
    with pytest.raises(BankAccountWithNumberNotFoundException):
        bank_account_service.get_bank_account_balance("1" * 20)


@pytest.mark.django_db
def test_get_bank_account_balance(create_bot_user1_with_bank_account1):
    assert bank_account_service.get_bank_account_balance("1" * 20) == 1000
