import pytest

from app.internal.money_transactions.domain.entities import MoneyTransactionIn
from tests.conftest import money_transaction_service


@pytest.mark.django_db
def test_get_all_usernames_which_user_interacted_with_returns_empty_list_when_no_interactions(
    create_bot_user1_with_bank_account1,
):
    assert list(money_transaction_service.get_all_usernames_which_user_interacted_with(1)) == []


@pytest.mark.django_db
def test_get_all_usernames_which_user_interacted_with_returns_one_username_after_one_transaction(
    create_bot_user1_with_bank_account1, create_bot_user2_with_bank_account2
):
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="2" * 20, money_amount=1
        )
    )
    assert list(money_transaction_service.get_all_usernames_which_user_interacted_with(1)) == ["second"]
    assert list(money_transaction_service.get_all_usernames_which_user_interacted_with(2)) == ["first"]


@pytest.mark.django_db
def test_get_all_usernames_which_user_interacted_with_returns_no_duplicates_if_there_are_many_transactions(
    create_bot_user1_with_bank_account1, create_bot_user2_with_bank_account2
):
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="2" * 20, money_amount=1
        )
    )
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="2" * 20, money_amount=1
        )
    )
    assert list(money_transaction_service.get_all_usernames_which_user_interacted_with(1)) == ["second"]
    assert list(money_transaction_service.get_all_usernames_which_user_interacted_with(2)) == ["first"]


@pytest.mark.django_db
def test_get_all_usernames_which_user_interacted_with_can_return_multiple_usernames(
    create_bot_user1_with_bank_account1, create_bot_user2_with_bank_account2
):
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="2" * 20, money_amount=1
        )
    )
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="1" * 20, money_amount=1
        )
    )
    assert set(money_transaction_service.get_all_usernames_which_user_interacted_with(1)) == {"second", "first"}
    assert list(money_transaction_service.get_all_usernames_which_user_interacted_with(2)) == ["first"]


@pytest.mark.django_db
def test_get_all_money_transactions_of_bank_account_returns_empty_list_when_user_not_interacted(create_bot_user1):
    assert list(money_transaction_service.get_all_money_transactions_of_bank_account("1" * 20)) == []


@pytest.mark.django_db
def test_get_all_money_transactions_of_bank_account_returns_one_transaction_when_user_made_one_transaction(
    create_bot_user1_with_bank_account1, create_bot_user2_with_bank_account2
):
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="2" * 20, money_amount=1
        )
    )
    transactions1 = money_transaction_service.get_all_money_transactions_of_bank_account("1" * 20)
    transactions2 = money_transaction_service.get_all_money_transactions_of_bank_account("2" * 20)
    assert len(transactions1) == 1 and len(transactions2) == 1
    transactions = [transactions1[0], transactions2[0]]
    for transaction in transactions:
        assert (
            transaction.money_amount == 1
            and transaction.sender_account_number == "1" * 20
            and transaction.receiver_account_number == "2" * 20
        )


@pytest.mark.django_db
def test_get_all_money_transactions_of_bank_account_returns_transactions_in_created_order(
    create_bot_user1_with_bank_account1, create_bot_user2_with_bank_account2
):
    for i in range(1, 11):
        money_transaction_service.send_money(
            MoneyTransactionIn(
                postcard_image_path="test",
                sender_account_number="1" * 20,
                receiver_account_number="2" * 20,
                money_amount=i,
            )
        )
    transactions = money_transaction_service.get_all_money_transactions_of_bank_account("1" * 20)
    for i, transaction in enumerate(transactions):
        assert transaction.money_amount == i + 1
