# import pytest
#
# from app.internal.services.models.user_service import get_user_by_id, get_user_by_name
#
#
# @pytest.mark.django_db
# def test_get_user_by_id_when_no_user():
#     assert get_user_by_id(1) is None
#
#
# @pytest.mark.django_db
# def test_get_user_by_id_when_user(create_bot_user1):
#     assert get_user_by_id(1) is not None
#
#
# @pytest.mark.django_db
# def test_get_user_by_name_when_no_user():
#     assert get_user_by_name("first") is None
#
#
# @pytest.mark.django_db
# def test_get_user_by_name_when_user():
#     user = get_user_by_id(1)
#     assert get_user_by_name("first") == user
