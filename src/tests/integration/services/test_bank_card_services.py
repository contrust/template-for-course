import pytest

from app.internal.exceptions.db.repositories import BankCardWithNumberNotFoundException
from tests.conftest import bank_card_service


@pytest.mark.django_db
def test_get_bank_card_when_no_card(create_bot_user1):
    with pytest.raises(BankCardWithNumberNotFoundException):
        bank_card_service.get_bank_card_by_number("1" * 15)


@pytest.mark.django_db
def test_get_bank_card_when_card(create_bot_user1_with_bank_card1):
    assert bank_card_service.get_bank_card_by_number("1" * 15) is not None


@pytest.mark.django_db
def test_get_all_bank_cards_when_no_cards(create_bot_user1_with_bank_account1):
    assert bank_card_service.get_all_bank_cards_numbers_of_bank_account("1" * 20) == []


@pytest.mark.django_db
def test_get_all_bank_cards_when_cards(create_bot_user1_with_bank_card1):
    assert bank_card_service.get_all_bank_cards_numbers_of_bank_account("1" * 20) == ["1" * 15]
