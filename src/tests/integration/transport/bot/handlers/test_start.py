import pytest

from app.internal.exceptions.db.repositories import BotUserWithIdNotFoundException
from app.internal.responses.presentation.bot_replies import USE_HELP, USER_SAVED
from tests.conftest import user_service
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_start_save_user_data_and_replies_user_saved_if_user_not_in_db(update, context):
    telegram_id = 321
    username = "joo16"
    first_name = "George"
    last_name = "Monta"
    update.effective_user.id = telegram_id
    update.effective_user.username = username
    update.effective_user.first_name = first_name
    update.effective_user.last_name = last_name
    with pytest.raises(BotUserWithIdNotFoundException):
        user_service.get_user_by_id(telegram_id)
    user_bot_handlers.start(update, context)
    user = user_service.get_user_by_id(telegram_id)
    assert (
        user.telegram_id == telegram_id
        and user.first_name == first_name
        and user.last_name == last_name
        and user.username == username
    )
    update.message.reply_text.assert_called_once_with("\n".join((USER_SAVED, USE_HELP)))


@pytest.mark.django_db
def test_start_updates_user_data_and_replies_user_saved_if_user_in_db(update, context, create_bot_user1_without_phone):
    telegram_id = 1
    user = user_service.get_user_by_id(telegram_id)
    username = "ark03"
    first_name = "Mike"
    last_name = "Black"
    update.effective_user.id = telegram_id
    update.effective_user.username = username
    update.effective_user.first_name = first_name
    update.effective_user.last_name = last_name
    user_bot_handlers.start(update, context)
    result_user = user_service.get_user_by_id(telegram_id)
    user.username = username
    user.last_name = last_name
    user.first_name = first_name
    assert result_user == user
    update.message.reply_text.assert_called_once_with("\n".join((USER_SAVED, USE_HELP)))
