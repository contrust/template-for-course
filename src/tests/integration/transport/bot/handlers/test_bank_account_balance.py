import pytest

from app.internal.responses.presentation.bot_states import END
from tests.conftest import bank_account_service
from tests.integration.transport.conftest import bank_account_bot_handlers


@pytest.mark.django_db
def test_bank_account_balance_replies_account_balance_and_returns_end_state(
    update, context, create_bot_user1_with_bank_account1
):
    bank_account_number = "1" * 20
    update.message.text = bank_account_number
    assert bank_account_bot_handlers.bank_account_balance(update, context) == END
    balance = bank_account_service.get_bank_account_balance(bank_account_number)
    update.message.reply_text.assert_called_once_with(str(balance))
