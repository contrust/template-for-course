import pytest

from app.internal.responses.presentation.bot_replies import NO_YOUR_BANK_ACCOUNT
from tests.integration.transport.conftest import bank_account_bot_handlers


@pytest.mark.django_db
def test_bank_account_list_replies_no_your_bank_accounts(update, context, create_bot_user1):
    bank_account_bot_handlers.bank_account_list(update, context)
    update.message.reply_text.assert_called_once_with(NO_YOUR_BANK_ACCOUNT)


@pytest.mark.django_db
def test_bank_account_list_replies_bank_account_numbers_line_by_line_when_there_are_accounts(
    update, context, create_bot_user1_with_bank_account1
):
    bank_account_bot_handlers.bank_account_list(update, context)
    update.message.reply_text.assert_called_once_with("1" * 20)
