from app.internal.responses.presentation.bot_replies import COMMANDS
from tests.integration.transport.conftest import docs_bot_handlers


def test_show_commands_replies_commands(update, context):
    docs_bot_handlers.show_commands(update, context)
    update.message.reply_text.assert_called_once_with(COMMANDS)
