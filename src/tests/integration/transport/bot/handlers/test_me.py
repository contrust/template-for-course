import pytest

from tests.conftest import user_service
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_me_replies_user_fields_line_by_line(update, context, create_bot_user1):
    user = user_service.get_user_by_id(1)
    user_bot_handlers.me(update, context)
    update.message.reply_text.assert_called_once_with(
        "\n".join((f"{key}: {value}" for key, value in user.dict().items()))
    )
