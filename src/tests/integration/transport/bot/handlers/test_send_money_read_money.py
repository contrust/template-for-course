# import pytest
#
# from app.internal.responses.presentation.bot_replies import MONEY_SENT, NOT_ENOUGH_MONEY, TRY_AGAIN_OR_CANCEL
# from app.internal.responses.presentation.bot_states import END, READ_MONEY
# from tests.conftest import bank_account_service
# from tests.integration.transport.conftest import money_transaction_bot_handlers
#
#
# @pytest.mark.django_db
# def test_send_money_read_money_sends_money_and_replies_money_sent_and_returns_end_when_enough_money(
#     update, context, create_bot_user1_with_bank_card1, create_bot_user2_with_bank_account2
# ):
#     context.user_data = {
#         "receiver_account_number": "2" * 20,
#         "sender_account_number": "1" * 20,
#     }
#     update.message.text = "500"
#     assert money_transaction_bot_handlers.send_money_read_money(update, context) == END
#     update.message.reply_text.assert_called_once_with(MONEY_SENT)
#     assert (
#         bank_account_service.get_bank_account_balance("1" * 20) == 500
#         and bank_account_service.get_bank_account_balance("2" * 20) == 1500
#     )
#
#
# @pytest.mark.django_db
# def test_send_money_read_money_doesnt_change_money_and_replies_no_money_and_returns_same_state_when_no_money(
#     update, context, create_bot_user1_with_bank_card1, create_bot_user2_with_bank_account2
# ):
#     context.user_data = {
#         "receiver_account_number": "2" * 20,
#         "sender_account_number": "1" * 20,
#     }
#     update.message.text = "1001"
#     assert money_transaction_bot_handlers.send_money_read_money(update, context) == READ_MONEY
#     update.message.reply_text.assert_called_once_with("\n".join((NOT_ENOUGH_MONEY, TRY_AGAIN_OR_CANCEL)))
#     assert (
#         bank_account_service.get_bank_account_balance("1" * 20) == 1000
#         and bank_account_service.get_bank_account_balance("2" * 20) == 1000
#     )
