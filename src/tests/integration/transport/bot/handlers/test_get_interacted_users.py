import pytest

from app.internal.money_transactions.domain.entities import MoneyTransactionIn
from app.internal.responses.presentation.bot_replies import NO_INTERACTED_USERS
from tests.conftest import money_transaction_service
from tests.integration.transport.conftest import money_transaction_bot_handlers


@pytest.mark.django_db
def test_get_interacted_users_replies_no_interaction_if_user_not_interacted(update, context, create_bot_user1):
    money_transaction_bot_handlers.get_interacted_users(update, context)
    update.message.reply_text.assert_called_once_with(NO_INTERACTED_USERS)


@pytest.mark.django_db
def test_get_interacted_users_replies_interacted_users_if_user_interacted(
    update, context, create_bot_user1_with_bank_account1, create_bot_user2_with_bank_account2
):
    money_transaction_service.send_money(
        MoneyTransactionIn(
            postcard_image_path="test", sender_account_number="1" * 20, receiver_account_number="2" * 20, money_amount=1
        )
    )
    money_transaction_bot_handlers.get_interacted_users(update, context)
    update.message.reply_text.assert_called_once_with("second")
