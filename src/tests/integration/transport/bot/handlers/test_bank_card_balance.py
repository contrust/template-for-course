import pytest

from app.internal.responses.presentation.bot_states import END
from tests.conftest import bank_card_service
from tests.integration.transport.conftest import bank_card_bot_handlers


@pytest.mark.django_db
def test_bank_card_balance_replies_account_balance_and_returns_end_state(
    update, context, create_bot_user1_with_bank_card1
):
    bank_card_number = "1" * 15
    update.message.text = bank_card_number
    assert bank_card_bot_handlers.bank_card_balance(update, context) == END
    balance = bank_card_service.get_bank_card_balance(bank_card_number)
    update.message.reply_text.assert_called_once_with(str(balance))
