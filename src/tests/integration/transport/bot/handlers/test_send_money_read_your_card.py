import pytest

from app.internal.responses.presentation.bot_replies import ENTER_MONEY_TO_SEND
from app.internal.responses.presentation.bot_states import READ_MONEY
from tests.integration.transport.conftest import money_transaction_bot_handlers


@pytest.mark.django_db
def test_send_money_read_your_card_saves_card_in_context_and_replies_enter_money_and_returns_read_money_state(
    update, context, create_bot_user1_with_bank_card1
):
    bank_card_number = "1" * 15
    update.message.text = bank_card_number
    assert money_transaction_bot_handlers.send_money_read_your_card(update, context) == READ_MONEY
    update.message.reply_text.assert_called_once_with(ENTER_MONEY_TO_SEND)
    assert context.user_data["sender_account_number"] == "1" * 20
