import pytest

from app.internal.responses.presentation.bot_replies import USER_ADDED_TO_YOUR_FAVORITES
from app.internal.responses.presentation.bot_states import END
from tests.conftest import user_service
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_add_favorite_read_adds_user_to_favorites_and_replies_user_added_and_returns_end(
    update, context, create_bot_user1
):
    username = "first"
    update.message.text = username
    assert user_bot_handlers.add_favorite_read(update, context) == END
    favorites = user_service.get_user_favorites_usernames(1)
    assert len(favorites) == 1 and favorites[0] == username
    update.message.reply_text.assert_called_once_with(USER_ADDED_TO_YOUR_FAVORITES)
