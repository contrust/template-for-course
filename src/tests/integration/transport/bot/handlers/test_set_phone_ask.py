import pytest

from app.internal.responses.presentation.bot_replies import ENTER_PHONE_NUMBER
from app.internal.responses.presentation.bot_states import READ_PHONE
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_set_phone_ask_replies_enter_phone_and_returns_read_phone_state(
    update, context, create_bot_user1_without_phone
):
    assert user_bot_handlers.set_phone_ask(update, context) == READ_PHONE
    update.message.reply_text.assert_called_once_with(ENTER_PHONE_NUMBER)
