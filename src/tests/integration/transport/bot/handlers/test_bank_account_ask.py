import pytest

from app.internal.responses.presentation.bot_replies import ENTER_BANK_ACCOUNT
from app.internal.responses.presentation.bot_states import READ_ACCOUNT
from tests.integration.transport.conftest import bank_account_bot_handlers


@pytest.mark.django_db
def test_bank_account_ask_replies_enter_bank_account_and_returns_read_account_state(update, context, create_bot_user1):
    assert bank_account_bot_handlers.bank_account_ask(update, context) == READ_ACCOUNT
    update.message.reply_text.assert_called_once_with(ENTER_BANK_ACCOUNT)
