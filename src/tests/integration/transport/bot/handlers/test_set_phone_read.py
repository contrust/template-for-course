import pytest

from app.internal.responses.presentation.bot_replies import PHONE_NUMBER_SAVED
from app.internal.responses.presentation.bot_states import END
from tests.conftest import user_service
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_set_phone_read_saves_phone_and_replies_phone_number_saved_and_returns_end_state(
    update, context, create_bot_user1_without_phone
):
    telegram_id = update.effective_user.id
    phone_number = "+74015710395"
    update.message.text = phone_number
    user = user_service.get_user_by_id(telegram_id)
    assert user.phone_number != phone_number
    assert user_bot_handlers.set_phone_read(update, context) == END
    user = user_service.get_user_by_id(telegram_id)
    assert user.phone_number == phone_number
    update.message.reply_text.assert_called_once_with(PHONE_NUMBER_SAVED)
