import pytest

from app.internal.responses.presentation.bot_replies import ENTER_USER
from app.internal.responses.presentation.bot_states import READ_FAVORITE
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_favorite_ask_replies_enter_username_and_returns_read_favorite_state(update, context, create_bot_user1):
    assert user_bot_handlers.favorite_ask(update, context) == READ_FAVORITE
    update.message.reply_text.assert_called_once_with(ENTER_USER)
