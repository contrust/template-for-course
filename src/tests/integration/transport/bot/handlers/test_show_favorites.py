import pytest

from app.internal.responses.presentation.bot_replies import NO_YOUR_FAVORITES
from tests.conftest import user_service
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
def test_show_favorites_replies_no_favorites_if_user_has_no_favorites(update, context, create_bot_user1):
    user_bot_handlers.show_favorites(update, context)
    update.message.reply_text.assert_called_once_with(NO_YOUR_FAVORITES)


@pytest.mark.django_db
def test_show_favorites_replies_favorites_usernames_if_user_has_favorite(
    update, context, create_bot_user1, create_bot_user2
):
    user_service.add_favorite_user(1, "second")
    user_bot_handlers.show_favorites(update, context)
    update.message.reply_text.assert_called_once_with("second")
