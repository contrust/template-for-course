import pytest

from app.internal.responses.presentation.bot_replies import ENTER_YOUR_BANK_CARD
from app.internal.responses.presentation.bot_states import READ_CARD
from tests.integration.transport.conftest import bank_card_bot_handlers


@pytest.mark.django_db
def test_bank_card_ask_replies_enter_bank_card_and_returns_read_card_state(update, context, create_bot_user1):
    assert bank_card_bot_handlers.bank_card_ask(update, context) == READ_CARD
    update.message.reply_text.assert_called_once_with(ENTER_YOUR_BANK_CARD)
