import pytest

from app.internal.responses.presentation.bot_replies import BANK_ACCOUNT_NO_CARDS
from app.internal.responses.presentation.bot_states import END
from tests.integration.transport.conftest import bank_account_bot_handlers


@pytest.mark.django_db
def test_bank_account_cards_replies_no_cards_and_returns_end_state_if_account_has_no_cards(
    update, context, create_bot_user1_with_bank_account1
):
    update.message.text = "1" * 20
    assert bank_account_bot_handlers.bank_account_cards(update, context) == END
    update.message.reply_text.assert_called_once_with(BANK_ACCOUNT_NO_CARDS)


@pytest.mark.django_db
def test_bank_account_cards_replies_card_numbers_and_returns_end_if_account_has_cards(
    update, context, create_bot_user1_with_bank_card1
):
    bank_account_number = "1" * 20
    bank_card_number = "1" * 15
    update.message.text = bank_account_number
    assert bank_account_bot_handlers.bank_account_cards(update, context) == END
    update.message.reply_text.assert_called_once_with(bank_card_number)
