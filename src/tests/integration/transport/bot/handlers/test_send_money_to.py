import pytest

from app.internal.responses.presentation.bot_replies import (
    ENTER_YOUR_CARD_TO_SEND_MONEY,
)
from app.internal.responses.presentation.bot_states import READ_YOUR_CARD
from tests.integration.transport.conftest import money_transaction_bot_handlers


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, text",
    (
        (money_transaction_bot_handlers.send_money_to_account_read, "1" * 20),
        (money_transaction_bot_handlers.send_money_to_user_read, "first"),
        (money_transaction_bot_handlers.send_money_to_card_read, "1" * 15),
    ),
)
def test_handler_save_bank_account_in_context_and_replies_enter_your_card_and_returns_read_your_card_state(
    update, context, create_bot_user1_with_bank_card1, handler, text
):
    update.message.text = text
    assert handler(update, context) == READ_YOUR_CARD
    update.message.reply_text.assert_called_once_with(ENTER_YOUR_CARD_TO_SEND_MONEY)
    assert context.user_data["receiver_account_number"] == "1" * 20
