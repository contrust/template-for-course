from app.internal.bot import get_bot


def test_bot_token_is_valid():
    bot = get_bot()
    assert bot.dispatcher.bot.get_webhook_info()
