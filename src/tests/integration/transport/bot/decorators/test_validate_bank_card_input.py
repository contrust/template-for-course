import pytest

from app.internal.responses.presentation.bot_replies import (
    BANK_CARD_NUMBER_NOT_FOUND,
    BANK_CARD_NUMBER_WRONG_FORMAT,
    TRY_AGAIN_OR_CANCEL,
)
from app.internal.responses.presentation.bot_states import (
    READ_CARD,
    READ_CARD_TO_SEND_MONEY,
    READ_YOUR_CARD,
)
from tests.integration.transport.conftest import (
    bank_card_bot_handlers,
    money_transaction_bot_handlers,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (bank_card_bot_handlers.bank_card_balance, READ_CARD),
        (money_transaction_bot_handlers.send_money_to_card_read, READ_CARD_TO_SEND_MONEY),
        (money_transaction_bot_handlers.send_money_read_your_card, READ_YOUR_CARD),
    ),
)
def test_handler_replies_wrong_card_format_and_returns_same_state_if_input_card_wrong_format(
    update, context, create_bot_user1, handler, expected_state
):
    update.message.text = "1f"
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((BANK_CARD_NUMBER_WRONG_FORMAT, TRY_AGAIN_OR_CANCEL)))


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (bank_card_bot_handlers.bank_card_balance, READ_CARD),
        (money_transaction_bot_handlers.send_money_to_card_read, READ_CARD_TO_SEND_MONEY),
        (money_transaction_bot_handlers.send_money_read_your_card, READ_YOUR_CARD),
        (money_transaction_bot_handlers.bank_card_statement, READ_CARD),
    ),
)
def test_handler_replies_no_card_in_db_if_input_card_no_in_db(
    update, context, create_bot_user1, handler, expected_state
):
    update.message.text = "1" * 15
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((BANK_CARD_NUMBER_NOT_FOUND, TRY_AGAIN_OR_CANCEL)))
