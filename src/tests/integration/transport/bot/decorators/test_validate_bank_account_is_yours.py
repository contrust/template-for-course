import pytest

from app.internal.responses.presentation.bot_replies import (
    BANK_ACCOUNT_NUMBER_NOT_YOURS,
    TRY_AGAIN_OR_CANCEL,
)
from app.internal.responses.presentation.bot_states import READ_ACCOUNT
from tests.integration.transport.conftest import (
    bank_account_bot_handlers,
    money_transaction_bot_handlers,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (bank_account_bot_handlers.bank_account_balance, READ_ACCOUNT),
        (bank_account_bot_handlers.bank_account_cards, READ_ACCOUNT),
        (money_transaction_bot_handlers.bank_account_statement, READ_ACCOUNT),
    ),
)
def test_handler_replies_account_not_yours_and_returns_same_state_if_input_account_not_yours(
    update, context, create_bot_user1, create_bot_user2_with_bank_account2, handler, expected_state
):
    update.message.text = "2" * 20
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((BANK_ACCOUNT_NUMBER_NOT_YOURS, TRY_AGAIN_OR_CANCEL)))
