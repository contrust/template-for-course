import pytest

from app.internal.responses.presentation.bot_replies import (
    BANK_ACCOUNT_NOT_FOUND,
    BANK_ACCOUNT_NUMBER_WRONG_FORMAT,
    TRY_AGAIN_OR_CANCEL,
)
from app.internal.responses.presentation.bot_states import (
    READ_ACCOUNT,
    READ_ACCOUNT_TO_SEND_MONEY,
)
from tests.integration.transport.conftest import (
    bank_account_bot_handlers,
    money_transaction_bot_handlers,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (bank_account_bot_handlers.bank_account_balance, READ_ACCOUNT),
        (bank_account_bot_handlers.bank_account_cards, READ_ACCOUNT),
        (money_transaction_bot_handlers.send_money_to_account_read, READ_ACCOUNT_TO_SEND_MONEY),
    ),
)
def test_handler_replies_wrong_account_format_if_input_account_wrong(
    update, context, create_bot_user1, handler, expected_state
):
    update.message.text = "4371092f"
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with(
        "\n".join((BANK_ACCOUNT_NUMBER_WRONG_FORMAT, TRY_AGAIN_OR_CANCEL))
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (bank_account_bot_handlers.bank_account_balance, READ_ACCOUNT),
        (bank_account_bot_handlers.bank_account_cards, READ_ACCOUNT),
        (money_transaction_bot_handlers.send_money_to_account_read, READ_ACCOUNT_TO_SEND_MONEY),
        (money_transaction_bot_handlers.bank_account_statement, READ_ACCOUNT),
    ),
)
def test_handler_replies_account_not_found_and_returns_same_state_if_no_account_in_db(
    update, context, create_bot_user1, handler, expected_state
):
    update.message.text = "1" * 20
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((BANK_ACCOUNT_NOT_FOUND, TRY_AGAIN_OR_CANCEL)))
