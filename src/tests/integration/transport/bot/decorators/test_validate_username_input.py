import pytest

from app.internal.responses.presentation.bot_replies import (
    NO_USER_IN_DATABASE,
    TRY_AGAIN_OR_CANCEL,
)
from app.internal.responses.presentation.bot_states import (
    READ_FAVORITE,
    READ_USER_TO_SEND_MONEY,
)
from tests.integration.transport.conftest import (
    money_transaction_bot_handlers,
    user_bot_handlers,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (user_bot_handlers.add_favorite_read, READ_FAVORITE),
        (user_bot_handlers.remove_favorite_read, READ_FAVORITE),
        (money_transaction_bot_handlers.send_money_to_user_read, READ_USER_TO_SEND_MONEY),
    ),
)
def test_handler_replies_no_user_in_db_and_returns_same_state_if_input_user_no_in_db(
    update, context, create_bot_user1, handler, expected_state
):
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((NO_USER_IN_DATABASE, TRY_AGAIN_OR_CANCEL)))
