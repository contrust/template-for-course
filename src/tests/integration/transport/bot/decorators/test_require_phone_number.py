import pytest

from app.internal.responses.presentation.bot_replies import PHONE_NUMBER_REQUIRED
from app.internal.responses.presentation.bot_states import (
    END,
    READ_ACCOUNT,
    READ_ACCOUNT_TO_SEND_MONEY,
    READ_CARD,
    READ_CARD_TO_SEND_MONEY,
    READ_FAVORITE,
    READ_MONEY,
    READ_PHONE,
    READ_USER_TO_SEND_MONEY,
    READ_YOUR_CARD,
)
from tests.integration.transport.conftest import (
    bank_account_bot_handlers,
    bank_card_bot_handlers,
    money_transaction_bot_handlers,
    user_bot_handlers,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler",
    (
        bank_account_bot_handlers.bank_account_balance,
        bank_account_bot_handlers.bank_account_cards,
        bank_card_bot_handlers.bank_card_balance,
        user_bot_handlers.add_favorite_read,
        user_bot_handlers.remove_favorite_read,
        money_transaction_bot_handlers.send_money_to_card_read,
        money_transaction_bot_handlers.send_money_to_account_read,
        money_transaction_bot_handlers.send_money_to_user_read,
        money_transaction_bot_handlers.send_money_read_your_card,
        money_transaction_bot_handlers.send_money_read_money,
        money_transaction_bot_handlers.get_interacted_users,
        money_transaction_bot_handlers.bank_card_statement,
        money_transaction_bot_handlers.bank_account_statement,
    ),
)
def test_handler_replies_phone_number_required_and_returns_same_state_if_user_has_no_phone_number(
    update, context, create_bot_user1_without_phone, handler
):
    assert handler(update, context) == END
    update.message.reply_text.assert_called_once_with(PHONE_NUMBER_REQUIRED)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (user_bot_handlers.set_phone_read, READ_PHONE),
        (bank_account_bot_handlers.bank_account_balance, READ_ACCOUNT),
        (bank_account_bot_handlers.bank_account_cards, READ_ACCOUNT),
        (bank_card_bot_handlers.bank_card_balance, READ_CARD),
        (user_bot_handlers.add_favorite_read, READ_FAVORITE),
        (user_bot_handlers.remove_favorite_read, READ_FAVORITE),
        (money_transaction_bot_handlers.send_money_to_card_read, READ_CARD_TO_SEND_MONEY),
        (money_transaction_bot_handlers.send_money_to_account_read, READ_ACCOUNT_TO_SEND_MONEY),
        (money_transaction_bot_handlers.send_money_to_user_read, READ_USER_TO_SEND_MONEY),
        (money_transaction_bot_handlers.send_money_read_your_card, READ_YOUR_CARD),
        (money_transaction_bot_handlers.send_money_read_money, READ_MONEY),
    ),
)
def test_handler_returns_same_state_if_message_is_none(update, context, create_bot_user1, handler, expected_state):
    update.message = None
    assert handler(update, context) == expected_state
