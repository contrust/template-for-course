import pytest

from app.internal.responses.presentation.bot_replies import (
    PHONE_NUMBER_WRONG_FORMAT,
    TRY_AGAIN_OR_CANCEL,
)
from app.internal.responses.presentation.bot_states import READ_PHONE
from tests.integration.transport.conftest import user_bot_handlers


@pytest.mark.django_db
@pytest.mark.parametrize("handler, expected_state", ((user_bot_handlers.set_phone_read, READ_PHONE),))
def test_handler_replies_wrong_phone_number_and_returns_same_state_if_input_is_wrong(
    update, context, create_bot_user1_without_phone, handler, expected_state
):
    update.message.text = "84015710395"
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((PHONE_NUMBER_WRONG_FORMAT, TRY_AGAIN_OR_CANCEL)))
