import pytest

from app.internal.responses.presentation.bot_replies import (
    BANK_CARD_NUMBER_NOT_YOURS,
    TRY_AGAIN_OR_CANCEL,
)
from app.internal.responses.presentation.bot_states import READ_CARD, READ_YOUR_CARD
from tests.integration.transport.conftest import (
    bank_card_bot_handlers,
    money_transaction_bot_handlers,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler, expected_state",
    (
        (bank_card_bot_handlers.bank_card_balance, READ_CARD),
        (money_transaction_bot_handlers.send_money_read_your_card, READ_YOUR_CARD),
        (money_transaction_bot_handlers.bank_card_statement, READ_CARD),
    ),
)
def test_handler_replies_not_yours_card_and_returns_same_state_if_input_card_not_yours(
    update, context, create_bot_user1, create_bot_user2_with_bank_card2, handler, expected_state
):
    update.message.text = "2" * 15
    assert handler(update, context) == expected_state
    update.message.reply_text.assert_called_once_with("\n".join((BANK_CARD_NUMBER_NOT_YOURS, TRY_AGAIN_OR_CANCEL)))
