import pytest

from app.internal.transport.bot.handlers import (
    add_favorite_read,
    bank_account_ask,
    bank_account_balance,
    bank_account_cards,
    bank_account_list,
    bank_account_statement,
    bank_card_ask,
    bank_card_balance,
    bank_card_statement,
    favorite_ask,
    get_interacted_users,
    me,
    remove_favorite_read,
    send_money_read_money,
    send_money_read_your_card,
    send_money_to_account_ask,
    send_money_to_account_read,
    send_money_to_card_ask,
    send_money_to_card_read,
    send_money_to_user_ask,
    send_money_to_user_read,
    set_phone_ask,
    set_phone_read,
    show_favorites,
)
from app.internal.transport.bot.replies import NO_YOUR_RECORD, PHONE_NUMBER_REQUIRED
from app.internal.transport.bot.states import (
    END,
    READ_ACCOUNT,
    READ_ACCOUNT_TO_SEND_MONEY,
    READ_CARD,
    READ_CARD_TO_SEND_MONEY,
    READ_FAVORITE,
    READ_MONEY,
    READ_PHONE,
    READ_USER_TO_SEND_MONEY,
    READ_YOUR_CARD,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler",
    (
        set_phone_ask,
        set_phone_read,
        me,
        bank_account_list,
        bank_account_ask,
        bank_account_balance,
        bank_account_cards,
        bank_card_ask,
        bank_card_balance,
        show_favorites,
        favorite_ask,
        add_favorite_read,
        remove_favorite_read,
        send_money_to_card_ask,
        send_money_to_card_read,
        send_money_to_account_ask,
        send_money_to_account_read,
        send_money_to_user_ask,
        send_money_to_user_read,
        send_money_read_your_card,
        send_money_read_money,
        get_interacted_users,
        bank_card_statement,
        bank_account_statement,
    ),
)
def test_handler_replies_no_your_record_and_returns_same_message_if_no_user_in_db(update, context, handler):
    next_state = handler(update, context)
    assert next_state in (END, None)
    update.message.reply_text.assert_called_once_with(NO_YOUR_RECORD)
