from unittest import mock

import pytest


@pytest.fixture
def update(text="", telegram_id=1, username="", first_name="", last_name=""):
    mocked_update = mock.MagicMock()
    mocked_update.message.text = text
    mocked_update.effective_user.id = telegram_id
    mocked_update.effective_user.username = username
    mocked_update.effective_user.first_name = first_name
    mocked_update.effective_user.last_name = last_name
    yield mocked_update


@pytest.fixture
def context(user_data=None):
    if user_data is None:
        user_data = {}
    mocked_context = mock.MagicMock()
    mocked_context.user_data.__setitem__.side_effect = user_data.__setitem__
    mocked_context.user_data.__getitem__.side_effect = user_data.__getitem__
    return mocked_context
