from app.internal.auth.presentation.handlers import AuthBotHandlers
from app.internal.bank_accounts.presentation.handlers import BankAccountBotHandlers
from app.internal.bank_cards.presentation.handlers import BankCardBotHandlers
from app.internal.bot_users.presentation.handlers import BotUserBotHandlers
from app.internal.conversation.presentation.handlers import ConversationBotHandlers
from app.internal.docs.presentation.handlers import DocsBotHandlers
from app.internal.money_transactions.presentation.handlers import (
    MoneyTransactionBotHandlers,
)
from tests.conftest import (
    POSTCARDS_ROOT_PATH,
    bank_account_service,
    bank_card_service,
    money_transaction_service,
    storage_service,
    token_service,
    user_service,
)

conversation_bot_handlers = ConversationBotHandlers()
docs_bot_handlers = DocsBotHandlers()
user_bot_handlers = BotUserBotHandlers(user_service)
bank_account_bot_handlers = BankAccountBotHandlers(user_service, bank_account_service, bank_card_service)
bank_card_bot_handlers = BankCardBotHandlers(user_service, bank_card_service)
auth_bot_handlers = AuthBotHandlers(user_service, token_service)
money_transaction_bot_handlers = MoneyTransactionBotHandlers(
    user_service,
    money_transaction_service,
    bank_account_service,
    bank_card_service,
    storage_service,
    POSTCARDS_ROOT_PATH,
)
