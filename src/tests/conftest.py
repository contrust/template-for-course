import logging
import os.path
import shutil
from pathlib import Path

import pytest
from django.core.files.storage import FileSystemStorage

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_cards.db.models import BankCard
from app.internal.bank_cards.db.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bot_users.db.models import BotUser
from app.internal.bot_users.db.repositories import BotUserRepository
from app.internal.bot_users.domain.services import BotUserService
from app.internal.jwt.domain.service import JwtService
from app.internal.money_transactions.db.repositories import MoneyTransactionRepository
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.tokens.db.repositories import IssuedTokenRepository
from app.internal.tokens.domain.service import IssuedTokenService
from app.storages.domain.services import StorageService
from config import settings

logging.disable(logging.CRITICAL)

TEST_DIR = "test_dir"
POSTCARDS_ROOT_PATH = "postcards"

user_repository = BotUserRepository(settings.PASSWORD_SALT)
bank_account_repository = BankAccountRepository()
bank_card_repository = BankCardRepository()
token_repository = IssuedTokenRepository()
money_transaction_repository = MoneyTransactionRepository()

user_service = BotUserService(user_repository)
bank_account_service = BankAccountService(bank_account_repository)
bank_card_service = BankCardService(bank_card_repository)
jwt_service = JwtService(
    settings.JWT_SECRET,
    settings.ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES,
    settings.REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES,
)
token_service = IssuedTokenService(token_repository, jwt_service)
money_transaction_service = MoneyTransactionService(money_transaction_repository, bank_account_repository)
storage_service = StorageService(FileSystemStorage(base_url=TEST_DIR))


@pytest.fixture
def create_bot_user1_without_phone():
    user = BotUser(telegram_id=1, username="first")
    user.save()
    return user


@pytest.fixture
def create_bot_user1():
    user = BotUser(telegram_id=1, username="first", phone_number="+79501239065")
    user.save()
    return user


@pytest.fixture
def create_bot_user2():
    user = BotUser(telegram_id=2, username="second", phone_number="+795143317588")
    user.save()
    return user


@pytest.fixture
def create_bot_user1_with_bank_account1(create_bot_user1):
    bank_account = BankAccount(number="1" * 20, balance=1000, owner_id=create_bot_user1.telegram_id)
    bank_account.save()
    return bank_account


@pytest.fixture
def create_bot_user2_with_bank_account2(create_bot_user2):
    bank_account = BankAccount(number="2" * 20, balance=1000, owner_id=create_bot_user2.telegram_id)
    bank_account.save()
    return bank_account


@pytest.fixture
def create_bot_user1_with_bank_card1(create_bot_user1_with_bank_account1):
    bank_card = BankCard(number="1" * 15, account_id=create_bot_user1_with_bank_account1.id)
    bank_card.save()
    return bank_card


@pytest.fixture
def create_bot_user2_with_bank_card2(create_bot_user2_with_bank_account2):
    bank_card = BankCard(number="2" * 15, account_id=create_bot_user2_with_bank_account2.id)
    bank_card.save()
    return bank_card
