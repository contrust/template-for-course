from django.contrib import admin

from app.internal.admin_users.presentation.admin import AdminUserAdmin
from app.internal.bank_accounts.presentation.admin import BankAccountAdmin
from app.internal.bank_cards.presentation.admin import BankCardAdmin
from app.internal.bot_users.presentation.admin import BotUserAdmin
from app.internal.money_transactions.presentation.admin import MoneyTransactionAdmin
from app.internal.tokens.presentation.admin import IssuedTokenAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
