import decimal

from django.db import transaction
from django.db.models import F, Q, Sum

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.exceptions.db.repositories import (
    BankAccountNotFoundException,
    BankAccountWithNumberNotFoundException,
)
from app.internal.money_transactions.db.models import MoneyTransaction
from app.internal.money_transactions.domain.entities import (
    MoneyTransactionIn,
    MoneyTransactionOut,
)
from app.internal.money_transactions.domain.services import IMoneyTransactionRepository


class MoneyTransactionRepository(IMoneyTransactionRepository):
    def send_money(self, money_transaction_data: MoneyTransactionIn) -> MoneyTransactionOut:
        with transaction.atomic():
            updated_count = BankAccount.objects.filter(number=money_transaction_data.sender_account_number).update(
                balance=F("balance") - money_transaction_data.money_amount
            )
            if updated_count == 0:
                raise BankAccountWithNumberNotFoundException(money_transaction_data.sender_account_number)
            updated_count = BankAccount.objects.filter(number=money_transaction_data.receiver_account_number).update(
                balance=F("balance") + money_transaction_data.money_amount
            )
            if updated_count == 0:
                raise BankAccountWithNumberNotFoundException(money_transaction_data.receiver_account_number)
            sender_account_id = (
                BankAccount.objects.filter(number=money_transaction_data.sender_account_number)
                .values_list("id", flat=True)
                .first()
            )
            if sender_account_id is None:
                raise BankAccountWithNumberNotFoundException(money_transaction_data.sender_account_number)
            receiver_account_id = (
                BankAccount.objects.filter(number=money_transaction_data.receiver_account_number)
                .values_list("id", flat=True)
                .first()
            )
            if receiver_account_id is None:
                raise BankAccountWithNumberNotFoundException(money_transaction_data.receiver_account_number)
            money_transaction: MoneyTransaction = MoneyTransaction.objects.create(
                postcard_image=money_transaction_data.postcard_image_path,
                sender_account_id=sender_account_id,
                receiver_account_id=receiver_account_id,
                money_amount=money_transaction_data.money_amount,
            )
            return MoneyTransactionOut(
                postcard_image=money_transaction.postcard_image.url,
                date=money_transaction.date,
                sender_account_number=money_transaction_data.sender_account_number,
                receiver_account_number=money_transaction_data.receiver_account_number,
                money_amount=money_transaction_data.money_amount,
                read_by_receiver=money_transaction.read_by_receiver,
            )

    def get_all_usernames_which_user_interacted_with(self, telegram_id: int) -> list[str]:
        return list(
            MoneyTransaction.objects.filter(sender_account__owner_id=telegram_id)
            .values_list("receiver_account__owner__username", flat=True)
            .union(
                MoneyTransaction.objects.filter(receiver_account__owner_id=telegram_id).values_list(
                    "sender_account__owner__username", flat=True
                )
            )
        )

    def _get_money_transaction_out_from_orm(self, money_transaction_orm: MoneyTransaction) -> MoneyTransactionOut:
        return MoneyTransactionOut(
            postcard_image=money_transaction_orm.postcard_image.url,
            date=money_transaction_orm.date,
            sender_account_number=money_transaction_orm.sender_account.number,
            receiver_account_number=money_transaction_orm.receiver_account.number,
            money_amount=money_transaction_orm.money_amount,
            read_by_receiver=money_transaction_orm.read_by_receiver,
        )

    def read_all_received_money_transactions_of_user(self, telegram_id: int) -> None:
        MoneyTransaction.objects.filter(receiver_account__owner_id=telegram_id).update(read_by_receiver=True)

    def read_all_received_money_transactions_of_bank_account(self, bank_account_number: str) -> None:
        MoneyTransaction.objects.filter(receiver_account__number=bank_account_number).update(read_by_receiver=True)

    def get_all_unread_received_money_transactions_of_user(self, telegram_id: int) -> list[MoneyTransactionOut]:
        return [
            self._get_money_transaction_out_from_orm(money_transaction_orm)
            for money_transaction_orm in MoneyTransaction.objects.filter(
                read_by_receiver=False, receiver_account__owner_id=telegram_id
            )
            .select_related("receiver_account")
            .order_by("id")
        ]

    def get_all_money_transactions_of_bank_account(self, bank_account_number: str) -> list[MoneyTransactionOut]:
        return [
            self._get_money_transaction_out_from_orm(money_transaction_orm)
            for money_transaction_orm in MoneyTransaction.objects.filter(
                Q(sender_account__number=bank_account_number) | Q(receiver_account__number=bank_account_number)
            )
            .select_related("sender_account", "receiver_account")
            .order_by("id")
        ]

    def get_money_transaction_money_amount_total_sum(self) -> decimal.Decimal:
        total_sum = MoneyTransaction.objects.aggregate(TOTAL_MONEY_SUM=Sum("money_amount"))["TOTAL_MONEY_SUM"]
        return total_sum if total_sum is not None else 0
