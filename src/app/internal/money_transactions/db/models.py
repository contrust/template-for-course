from django.db import models

from app.internal.bank_accounts.db.models import BankAccount


class MoneyTransaction(models.Model):
    postcard_image = models.ImageField(default="")
    date = models.DateField(auto_now=True)
    sender_account = models.ForeignKey(BankAccount, related_name="send_transactions", on_delete=models.CASCADE)
    receiver_account = models.ForeignKey(BankAccount, related_name="receive_transactions", on_delete=models.CASCADE)
    money_amount = models.DecimalField(max_digits=14, decimal_places=2)
    read_by_receiver = models.BooleanField(default=False)
