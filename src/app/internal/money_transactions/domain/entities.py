import datetime
import decimal

from ninja import File, Schema, UploadedFile


class MoneyTransactionOut(Schema):
    postcard_image: str
    date: datetime.date
    sender_account_number: str
    receiver_account_number: str
    money_amount: decimal.Decimal
    read_by_receiver: bool


class MoneyTransactionInWithoutPostcard(Schema):
    sender_account_number: str
    receiver_account_number: str
    money_amount: decimal.Decimal


class MoneyTransactionIn(Schema):
    postcard_image_path: str
    sender_account_number: str
    receiver_account_number: str
    money_amount: decimal.Decimal
