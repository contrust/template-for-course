import decimal
from abc import ABC, abstractmethod

from app.internal.bank_accounts.domain.servicies import IBankAccountRepository
from app.internal.exceptions.db.repositories import (
    NotEnoughMoneyException,
    WrongAssignmentException,
)
from app.internal.money_transactions.domain.entities import (
    MoneyTransactionIn,
    MoneyTransactionOut,
)


class IMoneyTransactionRepository(ABC):
    @abstractmethod
    def send_money(self, money_transaction_data: MoneyTransactionIn) -> MoneyTransactionOut:
        pass

    @abstractmethod
    def get_all_usernames_which_user_interacted_with(self, telegram_id: int) -> list[str]:
        pass

    @abstractmethod
    def read_all_received_money_transactions_of_user(self, telegram_id: int) -> None:
        pass

    @abstractmethod
    def read_all_received_money_transactions_of_bank_account(self, bank_account_number: str) -> None:
        pass

    @abstractmethod
    def get_all_unread_received_money_transactions_of_user(self, telegram_id: int) -> list[MoneyTransactionOut]:
        pass

    @abstractmethod
    def get_all_money_transactions_of_bank_account(self, bank_account_number: str) -> list[MoneyTransactionOut]:
        pass

    @abstractmethod
    def get_money_transaction_money_amount_total_sum(self) -> decimal.Decimal:
        pass


class MoneyTransactionService:
    def __init__(
        self, money_transaction_repository: IMoneyTransactionRepository, bank_account_service: IBankAccountRepository
    ):
        self._money_transaction_repository = money_transaction_repository
        self._bank_account_repository = bank_account_service

    def does_sender_have_enough_money(self, money_transaction_data: MoneyTransactionIn) -> bool:
        return (
            self._bank_account_repository.get_bank_account_balance(money_transaction_data.sender_account_number)
            > money_transaction_data.money_amount
        )

    def send_money(self, money_transaction_data: MoneyTransactionIn) -> MoneyTransactionOut:
        if money_transaction_data.money_amount <= 0:
            raise WrongAssignmentException(
                "money_amount", money_transaction_data.money_amount, "Money amount should be a positive number."
            )
        if not self.does_sender_have_enough_money(money_transaction_data):
            raise NotEnoughMoneyException(
                money_transaction_data.sender_account_number, money_transaction_data.money_amount
            )
        return self._money_transaction_repository.send_money(money_transaction_data)

    def read_all_received_money_transactions_of_user(self, telegram_id: int) -> None:
        return self._money_transaction_repository.read_all_received_money_transactions_of_user(telegram_id)

    def read_all_received_money_transactions_of_bank_account(self, bank_account_number: str) -> None:
        return self._money_transaction_repository.read_all_received_money_transactions_of_bank_account(
            bank_account_number
        )

    def get_all_usernames_which_user_interacted_with(self, telegram_id: int) -> list[str]:
        return self._money_transaction_repository.get_all_usernames_which_user_interacted_with(telegram_id)

    def get_all_unread_received_money_transactions_of_user(self, telegram_id: int) -> list[MoneyTransactionOut]:
        return self._money_transaction_repository.get_all_unread_received_money_transactions_of_user(telegram_id)

    def get_all_money_transactions_of_bank_account(self, bank_account_number: str) -> list[MoneyTransactionOut]:
        return self._money_transaction_repository.get_all_money_transactions_of_bank_account(bank_account_number)

    def get_money_transaction_money_amount_total_sum(self) -> decimal.Decimal:
        return self._money_transaction_repository.get_money_transaction_money_amount_total_sum()
