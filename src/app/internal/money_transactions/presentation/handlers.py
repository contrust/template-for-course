import logging

from ninja import File, UploadedFile
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bot_users.domain.services import BotUserService
from app.internal.decorators.bot_handlers_decorators import (
    require_phone_number,
    validate_bank_account_input,
    validate_bank_account_is_yours,
    validate_bank_card_input,
    validate_bank_card_is_yours,
    validate_image_input,
    validate_money_input,
    validate_user_has_bank_account,
    validate_username_input,
)
from app.internal.exceptions.presentation.handlers import ForbiddenResourceException
from app.internal.money_transactions.domain.entities import (
    MoneyTransactionIn,
    MoneyTransactionInWithoutPostcard,
    MoneyTransactionOut,
)
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.responses.presentation.bot_replies import (
    ENTER_ACCOUNT_TO_SEND_MONEY,
    ENTER_BANK_ACCOUNT,
    ENTER_CARD_TO_SEND_MONEY,
    ENTER_MONEY_TO_SEND,
    ENTER_POSTCARD,
    ENTER_USER_TO_SEND_MONEY,
    ENTER_YOUR_BANK_CARD,
    ENTER_YOUR_CARD_TO_SEND_MONEY,
    MONEY_SENT,
    NO_BANK_ACCOUNT_TRANSACTIONS,
    NO_INTERACTED_USERS,
    NO_NEW_RECEIVED_TRANSACTIONS,
    NOT_ENOUGH_MONEY,
    TRY_AGAIN_OR_CANCEL,
    get_bank_account_statement_record_string,
)
from app.internal.responses.presentation.bot_states import (
    END,
    READ_ACCOUNT,
    READ_ACCOUNT_TO_SEND_MONEY,
    READ_CARD,
    READ_CARD_TO_SEND_MONEY,
    READ_IMAGE,
    READ_MONEY,
    READ_USER_TO_SEND_MONEY,
    READ_YOUR_CARD,
)
from app.storages.domain.services import StorageService

logger = logging.getLogger("tg")


class MoneyTransactionAPIHandlers:
    def __init__(
        self,
        money_transaction_service: MoneyTransactionService,
        bank_account_service: BankAccountService,
        storage_service: StorageService,
        postcards_root_path: str,
    ):
        self._money_transaction_service = money_transaction_service
        self._bank_account_service = bank_account_service
        self._storage_service = storage_service
        self._postcards_root_path = postcards_root_path

    def send_money(
        self,
        request,
        money_transaction_data: MoneyTransactionInWithoutPostcard,
        postcard_image: UploadedFile = File(...),
    ) -> MoneyTransactionOut:
        if not self._bank_account_service.does_user_have_bank_account(
            request.user_id, money_transaction_data.sender_account_number
        ):
            raise ForbiddenResourceException
        postcard_image_path = self._storage_service.get_alternative_image_name(self._postcards_root_path)
        self._storage_service.create(postcard_image_path, postcard_image.read())
        transaction = self._money_transaction_service.send_money(
            MoneyTransactionIn(
                postcard_image_path=postcard_image_path,
                receiver_account_number=money_transaction_data.receiver_account_number,
                sender_account_number=money_transaction_data.sender_account_number,
                money_amount=money_transaction_data.money_amount,
            )
        )
        logger.info(
            f"{transaction.sender_account_number} sent {transaction.money_amount} to {transaction.receiver_account_number}."
        )
        return transaction

    def get_all_usernames_which_user_interacted_with(self, request) -> list[str]:
        return [
            username if username is not None else "-"
            for username in self._money_transaction_service.get_all_usernames_which_user_interacted_with(
                request.user_id
            )
        ]

    def get_all_unread_received_money_transactions(self, request) -> list[MoneyTransactionOut]:
        transactions = self._money_transaction_service.get_all_unread_received_money_transactions_of_user(
            request.user_id
        )
        self._money_transaction_service.read_all_received_money_transactions_of_user(request.user_id)
        return transactions


class MoneyTransactionBotHandlers:
    def __init__(
        self,
        user_service: BotUserService,
        money_transaction_service: MoneyTransactionService,
        bank_account_service: BankAccountService,
        bank_card_service: BankCardService,
        storage_service: StorageService,
        postcards_root_path: str,
    ):
        self._user_service = user_service
        self._money_transaction_service = money_transaction_service
        self._bank_account_service = bank_account_service
        self._bank_card_service = bank_card_service
        self._storage_service = storage_service
        self._postcards_root_path = postcards_root_path

    @require_phone_number
    def send_money_to_card_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_CARD_TO_SEND_MONEY)
        return READ_CARD_TO_SEND_MONEY

    @require_phone_number
    @validate_bank_card_input(current_state=READ_CARD_TO_SEND_MONEY)
    def send_money_to_card_read(self, update: Update, context: CallbackContext) -> int:
        context.user_data["receiver_account_number"] = self._bank_card_service.get_bank_account_number_of_bank_card(
            update.message.text
        )
        update.message.reply_text(ENTER_YOUR_CARD_TO_SEND_MONEY)
        return READ_YOUR_CARD

    @require_phone_number
    def send_money_to_account_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_ACCOUNT_TO_SEND_MONEY)
        return READ_ACCOUNT_TO_SEND_MONEY

    @require_phone_number
    @validate_bank_account_input(current_state=READ_ACCOUNT_TO_SEND_MONEY)
    def send_money_to_account_read(self, update: Update, context: CallbackContext) -> int:
        context.user_data["receiver_account_number"] = update.message.text
        update.message.reply_text(ENTER_YOUR_CARD_TO_SEND_MONEY)
        return READ_YOUR_CARD

    @require_phone_number
    def send_money_to_user_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_USER_TO_SEND_MONEY)
        return READ_USER_TO_SEND_MONEY

    @require_phone_number
    @validate_username_input(current_state=READ_USER_TO_SEND_MONEY)
    @validate_user_has_bank_account(current_state=READ_USER_TO_SEND_MONEY)
    def send_money_to_user_read(self, update: Update, context: CallbackContext) -> int:
        context.user_data["receiver_account_number"] = self._bank_account_service.get_any_bank_account_number_of_user(
            update.message.text
        )
        update.message.reply_text(ENTER_YOUR_CARD_TO_SEND_MONEY)
        return READ_YOUR_CARD

    @require_phone_number
    @validate_bank_card_input(current_state=READ_YOUR_CARD)
    @validate_bank_card_is_yours(current_state=READ_YOUR_CARD)
    def send_money_read_your_card(self, update: Update, context: CallbackContext) -> int:
        context.user_data["sender_account_number"] = self._bank_card_service.get_bank_account_number_of_bank_card(
            update.message.text
        )
        update.message.reply_text(ENTER_MONEY_TO_SEND)
        return READ_MONEY

    @require_phone_number
    @validate_money_input(current_state=READ_MONEY)
    def send_money_read_money(self, update: Update, context: CallbackContext) -> int:
        context.user_data["money_amount"] = float(update.message.text)
        update.message.reply_text(ENTER_POSTCARD)
        return READ_IMAGE

    @require_phone_number
    @validate_image_input(current_state=READ_IMAGE)
    def send_money_read_postcard(self, update: Update, context: CallbackContext) -> int:
        money_amount = context.user_data["money_amount"]
        sender_account_number = context.user_data["sender_account_number"]
        receiver_account_number = context.user_data["receiver_account_number"]
        if self._bank_account_service.get_bank_account_balance(sender_account_number) < money_amount:
            update.message.reply_text(NOT_ENOUGH_MONEY)
            return END
        postcard_image_path = self._storage_service.get_alternative_image_name(self._postcards_root_path)
        transaction = self._money_transaction_service.send_money(
            MoneyTransactionIn(
                postcard_image_path=postcard_image_path,
                sender_account_number=sender_account_number,
                receiver_account_number=receiver_account_number,
                money_amount=money_amount,
            )
        )
        file_id = update.message.photo[-1].file_id
        file = context.bot.get_file(file_id)
        buf = bytearray()
        file.download_as_bytearray(buf)
        self._storage_service.create(postcard_image_path, buf)
        update.message.reply_text(MONEY_SENT)
        logger.info(
            f"{transaction.sender_account_number} sent {transaction.money_amount} to {transaction.receiver_account_number}."
        )
        return READ_IMAGE

    @require_phone_number
    def get_interacted_users(self, update: Update, context: CallbackContext) -> None:
        interacted_usernames = self._money_transaction_service.get_all_usernames_which_user_interacted_with(
            update.effective_user.id
        )
        text = (
            "\n".join(map(lambda name: name if name is not None else "-", interacted_usernames))
            if len(interacted_usernames) != 0
            else NO_INTERACTED_USERS
        )
        update.message.reply_text(text)

    @require_phone_number
    def get_all_unread_money_transactions(self, update: Update, context: CallbackContext) -> None:
        transactions = self._money_transaction_service.get_all_unread_received_money_transactions_of_user(
            update.effective_user.id
        )
        text = (
            "\n\n".join(
                (
                    get_bank_account_statement_record_string(
                        transaction.postcard_image,
                        transaction.date,
                        transaction.money_amount,
                        transaction.receiver_account_number,
                        transaction.sender_account_number,
                        transaction.receiver_account_number,
                    )
                    for transaction in transactions
                )
            )
            if len(transactions) != 0
            else NO_NEW_RECEIVED_TRANSACTIONS
        )
        self._money_transaction_service.read_all_received_money_transactions_of_user(update.effective_user.id)
        update.message.reply_text(text)

    def _show_bank_account_statement(self, update, bank_account_number):
        transactions = self._money_transaction_service.get_all_money_transactions_of_bank_account(bank_account_number)
        text = (
            "\n\n".join(
                (
                    get_bank_account_statement_record_string(
                        transaction.postcard_image,
                        transaction.date,
                        transaction.money_amount,
                        bank_account_number,
                        transaction.sender_account_number,
                        transaction.receiver_account_number,
                    )
                    for transaction in transactions
                )
            )
            if len(transactions) != 0
            else NO_BANK_ACCOUNT_TRANSACTIONS
        )
        self._money_transaction_service.read_all_received_money_transactions_of_bank_account(bank_account_number)
        update.message.reply_text(text)
        return END

    @require_phone_number
    def bank_card_statement_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_YOUR_BANK_CARD)
        return READ_CARD

    @require_phone_number
    @validate_bank_card_input(current_state=READ_CARD)
    @validate_bank_card_is_yours(current_state=READ_CARD)
    def bank_card_statement(self, update: Update, context: CallbackContext) -> int:
        return self._show_bank_account_statement(
            update, self._bank_card_service.get_bank_account_number_of_bank_card(update.message.text)
        )

    @require_phone_number
    def bank_account_statement_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_BANK_ACCOUNT)
        return READ_ACCOUNT

    @require_phone_number
    @validate_bank_account_input(current_state=READ_ACCOUNT)
    @validate_bank_account_is_yours(current_state=READ_ACCOUNT)
    def bank_account_statement(self, update: Update, context: CallbackContext) -> int:
        return self._show_bank_account_statement(update, update.message.text)
