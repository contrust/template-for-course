from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.prometheus.metrics import money_transaction_money_amount_sum


def set_money_transactions_prometheus_metrics_functions(money_transaction_service: MoneyTransactionService) -> None:
    money_transaction_money_amount_sum.set_function(
        money_transaction_service.get_money_transaction_money_amount_total_sum
    )
