from ninja import NinjaAPI, Router
from telegram.ext import (
    CommandHandler,
    ConversationHandler,
    Filters,
    MessageHandler,
    Updater,
)

from app.internal.conversation.presentation.handlers import ConversationBotHandlers
from app.internal.middlewares.auth_middlewares import HTTPJWTAuth
from app.internal.money_transactions.domain.entities import MoneyTransactionOut
from app.internal.money_transactions.presentation.handlers import (
    MoneyTransactionAPIHandlers,
    MoneyTransactionBotHandlers,
)
from app.internal.responses.domain.entities import ErrorMessageResponse
from app.internal.responses.presentation.bot_states import (
    READ_ACCOUNT,
    READ_ACCOUNT_TO_SEND_MONEY,
    READ_CARD,
    READ_CARD_TO_SEND_MONEY,
    READ_IMAGE,
    READ_MONEY,
    READ_USER_TO_SEND_MONEY,
    READ_YOUR_CARD,
)


def get_money_transactions_api_router(money_transaction_handlers: MoneyTransactionAPIHandlers, auth: HTTPJWTAuth):
    router = Router(tags=["money_transactions"], auth=auth)

    router.add_api_operation(
        "/",
        ["POST"],
        money_transaction_handlers.send_money,
        response={200: MoneyTransactionOut, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/interacted_users/",
        ["GET"],
        money_transaction_handlers.get_all_usernames_which_user_interacted_with,
        response={200: list[str]},
    )

    router.add_api_operation(
        "/unread/",
        ["GET"],
        money_transaction_handlers.get_all_unread_received_money_transactions,
        response={200: list[MoneyTransactionOut]},
    )

    return router


def add_money_transactions_api_router(
    api: NinjaAPI, money_transaction_handlers: MoneyTransactionAPIHandlers, auth: HTTPJWTAuth
):
    api.add_router("/money_transactions/", get_money_transactions_api_router(money_transaction_handlers, auth))


def add_money_transactions_bot_handlers(
    updater: Updater,
    money_transaction_handlers: MoneyTransactionBotHandlers,
    conversation_handlers: ConversationBotHandlers,
):
    updater.dispatcher.add_handler(CommandHandler("interacted_users", money_transaction_handlers.get_interacted_users))
    updater.dispatcher.add_handler(
        CommandHandler("unread_money_transactions", money_transaction_handlers.get_all_unread_money_transactions)
    )

    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("send_money_to_card", money_transaction_handlers.send_money_to_card_ask)],
            states={
                READ_CARD_TO_SEND_MONEY: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.send_money_to_card_read)
                ],
                READ_YOUR_CARD: [
                    MessageHandler(
                        Filters.text & ~Filters.command, money_transaction_handlers.send_money_read_your_card
                    )
                ],
                READ_MONEY: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.send_money_read_money)
                ],
                READ_IMAGE: [MessageHandler(Filters.photo, money_transaction_handlers.send_money_read_postcard)],
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[
                CommandHandler("send_money_to_account", money_transaction_handlers.send_money_to_account_ask)
            ],
            states={
                READ_ACCOUNT_TO_SEND_MONEY: [
                    MessageHandler(
                        Filters.text & ~Filters.command, money_transaction_handlers.send_money_to_account_read
                    )
                ],
                READ_YOUR_CARD: [
                    MessageHandler(
                        Filters.text & ~Filters.command, money_transaction_handlers.send_money_read_your_card
                    )
                ],
                READ_MONEY: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.send_money_read_money)
                ],
                READ_IMAGE: [MessageHandler(Filters.photo, money_transaction_handlers.send_money_read_postcard)],
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("send_money_to_user", money_transaction_handlers.send_money_to_user_ask)],
            states={
                READ_USER_TO_SEND_MONEY: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.send_money_to_user_read)
                ],
                READ_YOUR_CARD: [
                    MessageHandler(
                        Filters.text & ~Filters.command, money_transaction_handlers.send_money_read_your_card
                    )
                ],
                READ_MONEY: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.send_money_read_money)
                ],
                READ_IMAGE: [MessageHandler(Filters.photo, money_transaction_handlers.send_money_read_postcard)],
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[
                CommandHandler("bank_account_statement", money_transaction_handlers.bank_account_statement_ask)
            ],
            states={
                READ_ACCOUNT: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.bank_account_statement)
                ],
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("bank_card_statement", money_transaction_handlers.bank_card_statement_ask)],
            states={
                READ_CARD: [
                    MessageHandler(Filters.text & ~Filters.command, money_transaction_handlers.bank_card_statement)
                ],
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
