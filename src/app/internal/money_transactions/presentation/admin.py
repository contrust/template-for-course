from django.contrib import admin

from app.internal.money_transactions.db.models import MoneyTransaction


@admin.register(MoneyTransaction)
class MoneyTransactionAdmin(admin.ModelAdmin):
    pass
