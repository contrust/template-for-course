from ninja import Schema


class ErrorMessageResponse(Schema):
    message: str


class SuccessfulResponse(Schema):
    success: bool
