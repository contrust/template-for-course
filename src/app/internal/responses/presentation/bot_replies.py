import datetime
import decimal

COMMANDS = (
    "/help - show this message.\n"
    "/start - create or update data in the database.\n"
    "/set_phone - set phone number in order to do other commands.\n"
    "/me - show all your data.\n"
    "/bank_account_list - show all your bank accounts.\n"
    "/bank_account_balance - show balance on given bank account.\n"
    "/bank_account_cards - show all the cards attached to given bank account.\n"
    "/bank_card_balance - show balance on given bank card.\n"
    "/favorites - show all your favorite users.\n"
    "/add_favorite - add a user to the list of favorites.\n"
    "/remove_favorite - remove a user from the list of favorites.\n"
    "/send_money_to_card - send money to a bank card from your bank card.\n"
    "/send_money_to_account - send money to a bank account from your bank card.\n"
    "/send_money_to_user - send money to a user from your bank card.\n"
    "/interacted_users - show all users who you sent money to or received from ('-' means the user has no name).\n"
    "/bank_account_statement - show bank account statement.\n"
    "/bank_card_statement - show bank card's account statement.\n"
    "/unread_money_transactions - show all unread received money transactions.\n"
    "/set_password - set a password for logging in the app.\n"
    "/login - get new access and refresh tokens for logging in the app.\n"
    "/cancel - cancel a command which requires multiple steps."
)
USER_SAVED = "The information about you has been saved in the database."
USE_HELP = "Use /help to get the list of all possible commands."
NO_YOUR_RECORD = "There is no record about you in the database.\n" "Use /start to create a new one."
PHONE_NUMBER_REQUIRED = (
    "You can't use this command because you haven't specified your phone number.\n" "Use /set_phone to do it."
)
PHONE_NUMBER_WRONG_FORMAT = "Given phone number doesn't match the format."
TRY_AGAIN_OR_CANCEL = "Try again or use /cancel to cancel this command."
COMMAND_CANCELED = "The previous command has been canceled."
ENTER_PHONE_NUMBER = "Enter your phone number with leading '+' followed by 8 to 15 digits."
PHONE_NUMBER_SAVED = (
    "Your phone number has been saved in the database successfully.\n" "Now you can use /me to see your data."
)
ENTER_BANK_ACCOUNT = "Enter your bank account number with 20 digits in count."
NO_YOUR_BANK_ACCOUNT = "You don't have any bank account."
BANK_ACCOUNT_NUMBER_NOT_YOURS = "Given bank account number doesn't belong to you."
BANK_ACCOUNT_NUMBER_WRONG_FORMAT = "Given bank account number doesn't match the format."
BANK_ACCOUNT_NO_CARDS = "Given bank account doesn't have any cards attached."
ENTER_YOUR_BANK_CARD = "Enter your bank card number with digits from 8 to 15 in count."
BANK_CARD_NUMBER_NOT_FOUND = "Given bank card is not in the database."
BANK_CARD_NUMBER_NOT_YOURS = "Given bank card number doesn't belong to you."
BANK_CARD_NUMBER_WRONG_FORMAT = "Given bank card number doesn't match the format."
NO_YOUR_FAVORITES = "You don't have any favorite user."
ENTER_USER = "Enter a telegram username."
BANK_ACCOUNT_NOT_FOUND = "Given bank account is not in the database."
NO_USER_IN_DATABASE = "There is no such user in the database."
USER_ADDED_TO_YOUR_FAVORITES = "The user has been added to your list of favorites."
USER_REMOVED_FROM_YOUR_FAVORITES = "The user has been removed from your list of favorites."
MONEY_IS_NOT_POSITIVE_NUMBER = "Given money amount is not a positive number."
NOT_ENOUGH_MONEY = "Not enough money to do this."
ENTER_MONEY_TO_SEND = "Enter how much money you want to send with positive decimal number."
USER_NO_ACCOUNTS = "Given user has no any bank account."
ENTER_CARD_TO_SEND_MONEY = "Enter what bank card you want to send money to with digits from 8 to 15 in count."
ENTER_ACCOUNT_TO_SEND_MONEY = "Enter what bank account you want to send money to with 20 digits in count."
ENTER_USER_TO_SEND_MONEY = "Enter what user you want to send money to with their telegram username."
ENTER_YOUR_CARD_TO_SEND_MONEY = (
    "Enter a bank card number which you want to use to send money with digits from 8 to 15 in count."
)
MONEY_SENT = "You have successfully sent money."
NO_INTERACTED_USERS = "You haven't interacted with any user."
NO_BANK_ACCOUNT_TRANSACTIONS = "This bank account has no transactions."
NO_NEW_RECEIVED_TRANSACTIONS = "There are no new received money transactions."
ENTER_PASSWORD = "Enter a password with characters from 8 to 64 in count."
PASSWORD_WRONG_FORMAT = "Given password doesn't match the format."
PASSWORD_SET = "Password set successfully."
NO_PASSWORD = "You have no password. Set a password with /set_password before logging in."
ENTER_POSTCARD = "Enter a postcard by sending its image."
CANT_HANDLE_EXCEPTION = "Sorry, something went wrong. An error occurred while handling exception."


def get_bank_account_statement_record_string(
    postcard_image_path: str,
    date: datetime.date,
    money_amount: decimal.Decimal,
    bank_account_number: str,
    sender_bank_account_number: str,
    receiver_bank_account_number: str,
):
    return (
        f"{date} sent {money_amount} to {receiver_bank_account_number}\nPostcard: {postcard_image_path}"
        if sender_bank_account_number == bank_account_number
        else f"{date} received {money_amount} from {sender_bank_account_number}\n" f"Postcard: {postcard_image_path}"
    )
