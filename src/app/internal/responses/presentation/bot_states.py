from telegram.ext import ConversationHandler

END = ConversationHandler.END
(
    READ_PHONE,
    READ_FAVORITE,
    READ_ACCOUNT,
    READ_CARD,
    READ_CARD_TO_SEND_MONEY,
    READ_ACCOUNT_TO_SEND_MONEY,
    READ_USER_TO_SEND_MONEY,
    READ_YOUR_CARD,
    READ_MONEY,
    READ_PASSWORD,
    READ_IMAGE,
) = range(11)
