import django.core.validators
from django.core.validators import RegexValidator


def does_string_match_regex_validator(string: str, validator: RegexValidator):
    try:
        validator(string)
    except django.core.validators.ValidationError:
        return False
    return True
