from django.core.validators import RegexValidator

from app.internal.validators.regex_validator import does_string_match_regex_validator

phone_regex_string = r"^\+\d{8,15}$"
validation_error_message = "Phone number must be entered with leading '+' " "followed by digits from 8 to 15 in count."
phone_regex_validator = RegexValidator(regex=phone_regex_string, message=validation_error_message)


def does_phone_number_match_format(phone_number: str) -> bool:
    return does_string_match_regex_validator(phone_number, phone_regex_validator)
