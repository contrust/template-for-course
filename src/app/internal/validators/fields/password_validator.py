from django.core.validators import RegexValidator

from app.internal.validators.regex_validator import does_string_match_regex_validator

password_regex_validator = RegexValidator(
    regex=r"^[a-zA-z0-9]{8,64}$",
    message="Password must be entered with latin characters or digits " "from 8 to 64 in count.",
)


def does_password_match_format(password: str) -> bool:
    return does_string_match_regex_validator(password, password_regex_validator)
