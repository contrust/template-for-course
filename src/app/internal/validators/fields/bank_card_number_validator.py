from django.core.validators import RegexValidator

from app.internal.validators.regex_validator import does_string_match_regex_validator

bank_card_number_regex_string = r"^\d{8,19}$"
validation_error_message = "Bank card number must be entered with digits from 8 to 19 in count."
bank_card_number_validator = RegexValidator(regex=bank_card_number_regex_string, message=validation_error_message)


def does_bank_card_number_match_format(bank_card_number: str):
    return does_string_match_regex_validator(bank_card_number, bank_card_number_validator)
