from django.core.validators import RegexValidator

from app.internal.validators.regex_validator import does_string_match_regex_validator

bank_account_number_regex_string = r"^\d{20}$"
validation_error_message = "Bank account number must be entered with 20 digits in count."
bank_account_number_validator = RegexValidator(regex=bank_account_number_regex_string, message=validation_error_message)


def does_bank_account_number_match_format(bank_account_number: str):
    return does_string_match_regex_validator(bank_account_number, bank_account_number_validator)
