from django.core.validators import MinValueValidator

balance_validator = MinValueValidator(limit_value=0)
