from telegram import Update
from telegram.ext import CallbackContext

from app.internal.responses.presentation.bot_replies import COMMAND_CANCELED
from app.internal.responses.presentation.bot_states import END


class ConversationBotHandlers:
    def cancel(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(COMMAND_CANCELED)
        return END
