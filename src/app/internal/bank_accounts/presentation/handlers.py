import decimal

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank_accounts.domain.entities import (
    BankAccountBalance,
    BankAccountOut,
)
from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bot_users.domain.services import BotUserService
from app.internal.decorators.bot_handlers_decorators import (
    require_phone_number,
    validate_bank_account_input,
    validate_bank_account_is_yours,
)
from app.internal.exceptions.presentation.handlers import ForbiddenResourceException
from app.internal.money_transactions.domain.entities import MoneyTransactionOut
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.responses.presentation.bot_replies import (
    BANK_ACCOUNT_NO_CARDS,
    ENTER_BANK_ACCOUNT,
    NO_YOUR_BANK_ACCOUNT,
)
from app.internal.responses.presentation.bot_states import END, READ_ACCOUNT


class BankAccountAPIHandlers:
    def __init__(
        self,
        bank_account_service: BankAccountService,
        bank_card_service: BankCardService,
        money_transaction_service: MoneyTransactionService,
    ):
        self._bank_account_service = bank_account_service
        self._bank_card_service = bank_card_service
        self._money_transaction_service = money_transaction_service

    def get_bank_account_cards(self, request, bank_account_number: str) -> list[str]:
        if not self._bank_account_service.does_user_have_bank_account(request.user_id, bank_account_number):
            raise ForbiddenResourceException
        return self._bank_card_service.get_all_bank_cards_numbers_of_bank_account(bank_account_number)

    def get_bank_account_by_number(self, request, bank_account_number: str) -> BankAccountOut:
        if not self._bank_account_service.does_user_have_bank_account(request.user_id, bank_account_number):
            raise ForbiddenResourceException
        return self._bank_account_service.get_bank_account_by_number(bank_account_number)

    def get_bank_account_balance_by_number(self, request, bank_account_number: str) -> BankAccountBalance:
        if not self._bank_account_service.does_user_have_bank_account(request.user_id, bank_account_number):
            raise ForbiddenResourceException
        return BankAccountBalance(balance=self._bank_account_service.get_bank_account_balance(bank_account_number))

    def get_all_bank_accounts(self, request) -> list[str]:
        return self._bank_account_service.get_all_bank_accounts_numbers_of_user(request.user_id)

    def get_all_money_transactions(self, request, bank_account_number: str) -> list[MoneyTransactionOut]:
        if not self._bank_account_service.does_user_have_bank_account(request.user_id, bank_account_number):
            raise ForbiddenResourceException
        transactions = self._money_transaction_service.get_all_money_transactions_of_bank_account(bank_account_number)
        self._money_transaction_service.read_all_received_money_transactions_of_bank_account(bank_account_number)
        return transactions


class BankAccountBotHandlers:
    def __init__(
        self, user_service: BotUserService, bank_account_service: BankAccountService, bank_card_service: BankCardService
    ):
        self._user_service = user_service
        self._bank_account_service = bank_account_service
        self._bank_card_service = bank_card_service

    @require_phone_number
    def bank_account_list(self, update: Update, context: CallbackContext) -> None:
        bank_accounts = self._bank_account_service.get_all_bank_accounts_numbers_of_user(update.effective_user.id)
        text = "\n".join(map(str, bank_accounts)) if bank_accounts else NO_YOUR_BANK_ACCOUNT
        update.message.reply_text(text)

    @require_phone_number
    def bank_account_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_BANK_ACCOUNT)
        return READ_ACCOUNT

    @require_phone_number
    @validate_bank_account_input(current_state=READ_ACCOUNT)
    @validate_bank_account_is_yours(current_state=READ_ACCOUNT)
    def bank_account_balance(self, update: Update, context: CallbackContext) -> int:
        balance = self._bank_account_service.get_bank_account_balance(update.message.text)
        update.message.reply_text(str(balance))
        return END

    @require_phone_number
    @validate_bank_account_input(current_state=READ_ACCOUNT)
    @validate_bank_account_is_yours(current_state=READ_ACCOUNT)
    def bank_account_cards(self, update: Update, context: CallbackContext) -> int:
        bank_cards_numbers = self._bank_card_service.get_all_bank_cards_numbers_of_bank_account(update.message.text)
        text = "\n".join(map(str, bank_cards_numbers)) if bank_cards_numbers else BANK_ACCOUNT_NO_CARDS
        update.message.reply_text(text)
        return END
