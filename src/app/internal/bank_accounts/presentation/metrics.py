from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.prometheus.metrics import bank_account_balance_sum


def set_bank_account_prometheus_metrics_functions(bank_account_service: BankAccountService) -> None:
    bank_account_balance_sum.set_function(bank_account_service.get_bank_accounts_balances_total_sum)
