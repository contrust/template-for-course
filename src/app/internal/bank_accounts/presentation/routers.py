from ninja import NinjaAPI, Router
from telegram.ext import (
    CommandHandler,
    ConversationHandler,
    Filters,
    MessageHandler,
    Updater,
)

from app.internal.bank_accounts.domain.entities import (
    BankAccountBalance,
    BankAccountOut,
)
from app.internal.bank_accounts.presentation.handlers import (
    BankAccountAPIHandlers,
    BankAccountBotHandlers,
)
from app.internal.conversation.presentation.handlers import ConversationBotHandlers
from app.internal.middlewares.auth_middlewares import HTTPJWTAuth
from app.internal.money_transactions.domain.entities import MoneyTransactionOut
from app.internal.responses.domain.entities import ErrorMessageResponse
from app.internal.responses.presentation.bot_states import READ_ACCOUNT


def get_bank_accounts_api_router(bank_account_handlers: BankAccountAPIHandlers, auth: HTTPJWTAuth):
    router = Router(tags=["bank_accounts"], auth=auth)

    router.add_api_operation(
        "", ["GET"], bank_account_handlers.get_all_bank_accounts, response={200: list[str], 404: ErrorMessageResponse}
    )

    router.add_api_operation(
        "/{bank_account_number}/",
        ["GET"],
        bank_account_handlers.get_bank_account_by_number,
        response={200: BankAccountOut, 403: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/{bank_account_number}/balance/",
        ["GET"],
        bank_account_handlers.get_bank_account_balance_by_number,
        response={200: BankAccountBalance, 403: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/{bank_account_number}/cards/",
        ["GET"],
        bank_account_handlers.get_bank_account_cards,
        response={200: list[str], 403: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/{bank_account_number}/money_transactions/",
        ["GET"],
        bank_account_handlers.get_all_money_transactions,
        response={200: list[MoneyTransactionOut], 403: ErrorMessageResponse},
    )

    return router


def add_bank_accounts_api_router(api: NinjaAPI, user_handlers: BankAccountAPIHandlers, auth: HTTPJWTAuth):
    api.add_router("/bank_accounts/", get_bank_accounts_api_router(user_handlers, auth))


def add_bank_accounts_bot_handlers(
    updater: Updater,
    bank_account_bot_handlers: BankAccountBotHandlers,
    conversation_bot_handlers: ConversationBotHandlers,
):
    updater.dispatcher.add_handler(CommandHandler("bank_account_list", bank_account_bot_handlers.bank_account_list))
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("bank_account_cards", bank_account_bot_handlers.bank_account_ask)],
            states={
                READ_ACCOUNT: [
                    MessageHandler(Filters.text & ~Filters.command, bank_account_bot_handlers.bank_account_cards)
                ]
            },
            fallbacks=[CommandHandler("cancel", conversation_bot_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("bank_account_balance", bank_account_bot_handlers.bank_account_ask)],
            states={
                READ_ACCOUNT: [
                    MessageHandler(Filters.text & ~Filters.command, bank_account_bot_handlers.bank_account_balance)
                ]
            },
            fallbacks=[CommandHandler("cancel", conversation_bot_handlers.cancel)],
        )
    )
