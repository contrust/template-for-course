import decimal

from ninja import Schema


class BankAccountOut(Schema):
    number: str
    balance: decimal.Decimal
    owner_telegram_id: int


class BankAccountBalance(Schema):
    balance: decimal.Decimal
