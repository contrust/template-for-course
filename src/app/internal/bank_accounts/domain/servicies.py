import decimal
from abc import ABC, abstractmethod

from app.internal.bank_accounts.domain.entities import BankAccountOut


class IBankAccountRepository(ABC):
    @abstractmethod
    def get_bank_account_id_by_number(self, bank_account_number: str) -> int:
        pass

    @abstractmethod
    def get_bank_account_by_number(self, bank_account_number: str) -> BankAccountOut:
        pass

    @abstractmethod
    def get_bank_account_balance(self, bank_account_number: str) -> decimal.Decimal:
        pass

    @abstractmethod
    def does_bank_account_with_number_exist(self, bank_account_number: str) -> bool:
        pass

    @abstractmethod
    def does_user_have_bank_account(self, user_telegram_id: int, bank_account_number: str) -> bool:
        pass

    @abstractmethod
    def get_user_with_username_bank_accounts_count(self, username: str):
        pass

    @abstractmethod
    def get_all_bank_accounts_numbers_of_user(self, user_telegram_id: int) -> list[str]:
        pass

    @abstractmethod
    def get_any_bank_account_number_of_user(self, username: str) -> str:
        pass

    @abstractmethod
    def get_bank_accounts_balances_total_sum(self) -> decimal.Decimal:
        pass


class BankAccountService:
    def __init__(self, bank_account_repository: IBankAccountRepository):
        self._bank_account_repository = bank_account_repository

    def get_bank_account_id_by_number(self, bank_account_number: str) -> int:
        return self._bank_account_repository.get_bank_account_id_by_number(bank_account_number)

    def get_bank_account_by_number(self, bank_account_number: str) -> BankAccountOut:
        return self._bank_account_repository.get_bank_account_by_number(bank_account_number)

    def get_bank_account_balance(self, bank_account_number: str) -> decimal.Decimal:
        return self._bank_account_repository.get_bank_account_balance(bank_account_number)

    def does_bank_account_with_number_exist(self, bank_account_number: str) -> bool:
        return self._bank_account_repository.does_bank_account_with_number_exist(bank_account_number)

    def does_user_have_bank_account(self, user_telegram_id: int, bank_account_number: str) -> bool:
        return self._bank_account_repository.does_user_have_bank_account(user_telegram_id, bank_account_number)

    def get_user_with_username_bank_accounts_count(self, username: str):
        return self._bank_account_repository.get_user_with_username_bank_accounts_count(username)

    def get_all_bank_accounts_numbers_of_user(self, user_telegram_id: int) -> list[str]:
        return self._bank_account_repository.get_all_bank_accounts_numbers_of_user(user_telegram_id)

    def get_any_bank_account_number_of_user(self, username: str) -> str:
        return self._bank_account_repository.get_any_bank_account_number_of_user(username)

    def get_bank_accounts_balances_total_sum(self) -> decimal.Decimal:
        return self._bank_account_repository.get_bank_accounts_balances_total_sum()
