from django.db import models

from app.internal.bot_users.db.models import BotUser
from app.internal.validators.fields.balance_validator import balance_validator
from app.internal.validators.fields.bank_account_number_validator import (
    bank_account_number_validator,
)


class BankAccount(models.Model):
    number = models.CharField(unique=True, max_length=20, default="", validators=[bank_account_number_validator])
    balance = models.DecimalField(max_digits=14, decimal_places=2, validators=[balance_validator])
    owner = models.ForeignKey(BotUser, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.number}"
