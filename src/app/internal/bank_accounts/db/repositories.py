import decimal

from django.db.models import Sum

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_accounts.domain.entities import BankAccountOut
from app.internal.bank_accounts.domain.servicies import IBankAccountRepository
from app.internal.exceptions.db.repositories import (
    BankAccountWithNumberNotFoundException,
)


class BankAccountRepository(IBankAccountRepository):
    def get_bank_accounts_balances_total_sum(self) -> decimal.Decimal:
        total_sum = BankAccount.objects.aggregate(TOTAL_BALANCE_SUM=Sum("balance"))["TOTAL_BALANCE_SUM"]
        return total_sum if total_sum is not None else 0

    def get_bank_account_id_by_number(self, bank_account_number: str) -> int:
        bank_account_id = BankAccount.objects.filter(number=bank_account_number).values_list("id", flat=True).first()
        if bank_account_id is None:
            raise BankAccountWithNumberNotFoundException(bank_account_number)
        return bank_account_id

    def get_bank_account_by_number(self, bank_account_number: str) -> BankAccountOut:
        bank_account = (
            BankAccount.objects.filter(number=bank_account_number)
            .values_list("number", "balance", "owner__telegram_id")
            .first()
        )
        if bank_account is None:
            raise BankAccountWithNumberNotFoundException(bank_account_number)
        return BankAccountOut(number=bank_account[0], balance=bank_account[1], owner_telegram_id=bank_account[2])

    def get_bank_account_balance(self, bank_account_number: str) -> decimal.Decimal:
        balance = BankAccount.objects.filter(number=bank_account_number).values_list("balance", flat=True).first()
        if balance is None:
            raise BankAccountWithNumberNotFoundException(bank_account_number)
        return balance

    def does_bank_account_with_number_exist(self, bank_account_number: str) -> bool:
        return BankAccount.objects.filter(number=bank_account_number).exists()

    def does_user_have_bank_account(self, user_telegram_id: int, bank_account_number: str) -> bool:
        return BankAccount.objects.filter(number=bank_account_number, owner__telegram_id=user_telegram_id).exists()

    def get_user_with_username_bank_accounts_count(self, username: str):
        return BankAccount.objects.filter(owner__username=username).count()

    def get_all_bank_accounts_numbers_of_user(self, user_telegram_id: int) -> list[str]:
        return list(
            BankAccount.objects.filter(owner__telegram_id=user_telegram_id)
            .values_list("number", flat=True)
            .order_by("number")
        )

    def get_any_bank_account_number_of_user(self, username: str) -> str:
        return BankAccount.objects.filter(owner__username=username).values_list("number", flat=True).first()
