from django.conf import settings
from django.core.files.storage import default_storage
from telegram import Update
from telegram.ext import CallbackContext, Updater

from app.internal.auth.presentation.handlers import AuthBotHandlers
from app.internal.auth.presentation.routers import add_auth_bot_handlers
from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_accounts.presentation.handlers import BankAccountBotHandlers
from app.internal.bank_accounts.presentation.routers import (
    add_bank_accounts_bot_handlers,
)
from app.internal.bank_cards.db.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_cards.presentation.handlers import BankCardBotHandlers
from app.internal.bank_cards.presentation.routers import add_bank_cards_bot_handlers
from app.internal.bot_users.db.repositories import BotUserRepository
from app.internal.bot_users.domain.services import BotUserService
from app.internal.bot_users.presentation.handlers import BotUserBotHandlers
from app.internal.bot_users.presentation.routers import add_bot_users_bot_handlers
from app.internal.conversation.presentation.handlers import ConversationBotHandlers
from app.internal.docs.presentation.handlers import DocsBotHandlers
from app.internal.docs.presentation.routers import add_docs_bot_handlers
from app.internal.jwt.domain.service import JwtService
from app.internal.money_transactions.db.repositories import MoneyTransactionRepository
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.money_transactions.presentation.handlers import (
    MoneyTransactionBotHandlers,
)
from app.internal.money_transactions.presentation.routers import (
    add_money_transactions_bot_handlers,
)
from app.internal.responses.presentation.bot_replies import CANT_HANDLE_EXCEPTION
from app.internal.responses.presentation.bot_states import END
from app.internal.tokens.db.repositories import IssuedTokenRepository
from app.internal.tokens.domain.service import IssuedTokenService
from app.storages.domain.services import StorageService
from config.settings import TRANSACTION_POSTCARDS_ROOT_PATH


def error_handler(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(CANT_HANDLE_EXCEPTION)
    return END


def get_bot() -> Updater:
    updater = Updater(settings.TELEGRAM_BOT_TOKEN, use_context=True)

    updater.dispatcher.add_error_handler(error_handler)

    user_repository = BotUserRepository(settings.PASSWORD_SALT)
    bank_account_repository = BankAccountRepository()
    bank_card_repository = BankCardRepository()
    token_repository = IssuedTokenRepository()
    money_transaction_repository = MoneyTransactionRepository()

    user_service = BotUserService(user_repository)
    bank_account_service = BankAccountService(bank_account_repository)
    bank_card_service = BankCardService(bank_card_repository)
    jwt_service = JwtService(
        settings.JWT_SECRET,
        settings.ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES,
        settings.REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES,
    )
    token_service = IssuedTokenService(token_repository, jwt_service)
    money_transaction_service = MoneyTransactionService(money_transaction_repository, bank_account_repository)
    storage_service = StorageService(default_storage)

    conversation_handlers = ConversationBotHandlers()
    docs_handlers = DocsBotHandlers()
    user_handlers = BotUserBotHandlers(user_service)
    bank_account_handlers = BankAccountBotHandlers(user_service, bank_account_service, bank_card_service)
    bank_cards_handlers = BankCardBotHandlers(user_service, bank_card_service)
    auth_handlers = AuthBotHandlers(user_service, token_service)
    money_transaction_handlers = MoneyTransactionBotHandlers(
        user_service,
        money_transaction_service,
        bank_account_service,
        bank_card_service,
        storage_service,
        TRANSACTION_POSTCARDS_ROOT_PATH,
    )

    add_docs_bot_handlers(updater, docs_handlers)
    add_bot_users_bot_handlers(updater, user_handlers, conversation_handlers)
    add_bank_accounts_bot_handlers(updater, bank_account_handlers, conversation_handlers)
    add_bank_cards_bot_handlers(updater, bank_cards_handlers, conversation_handlers)
    add_auth_bot_handlers(updater, auth_handlers)
    add_money_transactions_bot_handlers(updater, money_transaction_handlers, conversation_handlers)
    return updater


bot = get_bot()
