from app.internal.converters.hash_converters import convert_string_to_md5_hex_string
from app.internal.tokens.db.models import IssuedToken
from app.internal.tokens.domain.entities import IssuedTokenOut
from app.internal.tokens.domain.service import IIssuedTokenRepository


class IssuedTokenRepository(IIssuedTokenRepository):
    def create_token(self, jwt_token: str, user_telegram_id: int) -> IssuedTokenOut:
        return IssuedToken.objects.create(jwi=convert_string_to_md5_hex_string(jwt_token), user_id=user_telegram_id)

    def does_refresh_token_exist(self, jwt_refresh_token: str) -> bool:
        return IssuedToken.objects.filter(jwi=convert_string_to_md5_hex_string(jwt_refresh_token)).exists()

    def is_refresh_token_revoked(self, jwt_refresh_token: str) -> bool:
        return IssuedToken.objects.values_list("revoked", flat=True).get(
            jwi=convert_string_to_md5_hex_string(jwt_refresh_token)
        )

    def revoke_all_refresh_tokens_of_user(self, telegram_id: int) -> None:
        IssuedToken.objects.filter(user_id=telegram_id).update(revoked=True)
