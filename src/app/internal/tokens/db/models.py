from django.db import models

from app.internal.bot_users.db.models import BotUser


class IssuedToken(models.Model):
    jwi = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(BotUser, related_name="refresh_tokens", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)
