from ninja import NinjaAPI, Router

from app.internal.middlewares.auth_middlewares import HTTPJWTAuth
from app.internal.responses.domain.entities import ErrorMessageResponse
from app.internal.tokens.domain.entities import TokenPair
from app.internal.tokens.presentation.handlers import IssuedTokenAPIHandlers


def get_tokens_api_router(token_handlers: IssuedTokenAPIHandlers):
    router = Router(tags=["tokens"])

    router.add_api_operation(
        "/update/",
        ["POST"],
        token_handlers.update_access_token,
        response={200: TokenPair, 401: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    return router


def add_tokens_api_router(api: NinjaAPI, token_handlers: IssuedTokenAPIHandlers):
    api.add_router("/tokens/", get_tokens_api_router(token_handlers))
