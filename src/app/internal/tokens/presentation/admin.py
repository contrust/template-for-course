from django.contrib import admin

from app.internal.tokens.db.models import IssuedToken


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    pass
