from app.internal.exceptions.presentation.handlers import UnauthorizedException
from app.internal.tokens.domain.entities import RefreshToken, TokenPair
from app.internal.tokens.domain.service import IssuedTokenService


class IssuedTokenAPIHandlers:
    def __init__(self, issued_token_service: IssuedTokenService):
        self._issued_token_service = issued_token_service

    def update_access_token(self, request, refresh_token_data: RefreshToken) -> TokenPair:
        if not self._issued_token_service.does_refresh_token_exist(refresh_token_data.refresh):
            raise UnauthorizedException
        if self._issued_token_service.is_refresh_token_revoked(refresh_token_data.refresh):
            self._issued_token_service.revoke_all_refresh_tokens_of_user(request.user_id)
            raise UnauthorizedException
        decoded_jwt = self._issued_token_service._jwt_service.decode_jwt_token(refresh_token_data.refresh)
        return self._issued_token_service.generate_new_token_pair(decoded_jwt["id"])
