from abc import ABC, abstractmethod

from app.internal.jwt.domain.service import JwtService
from app.internal.tokens.domain.entities import IssuedTokenOut, TokenPair


class IIssuedTokenRepository:
    @abstractmethod
    def create_token(self, jwt_token: str, user_telegram_id: int) -> IssuedTokenOut:
        pass

    @abstractmethod
    def does_refresh_token_exist(self, jwt_refresh_token: str) -> bool:
        pass

    @abstractmethod
    def is_refresh_token_revoked(self, jwt_refresh_token: str) -> bool:
        pass

    @abstractmethod
    def revoke_all_refresh_tokens_of_user(self, telegram_id: int) -> None:
        pass


class IssuedTokenService:
    def __init__(self, issued_token_repository: IIssuedTokenRepository, jwt_service: JwtService):
        self._issued_token_repository = issued_token_repository
        self._jwt_service = jwt_service

    def create_token(self, jwt_token: str, user_telegram_id: int) -> IssuedTokenOut:
        return self._issued_token_repository.create_token(jwt_token, user_telegram_id)

    def revoke_all_refresh_tokens_of_user(self, user_telegram_id: int) -> None:
        self._issued_token_repository.revoke_all_refresh_tokens_of_user(user_telegram_id)

    def does_refresh_token_exist(self, jwt_refresh_token: str) -> bool:
        return self._issued_token_repository.does_refresh_token_exist(jwt_refresh_token)

    def is_refresh_token_revoked(self, jwt_refresh_token: str) -> bool:
        return self._issued_token_repository.is_refresh_token_revoked(jwt_refresh_token)

    def generate_refresh_token(self, user_telegram_id: int) -> str:
        token = self._jwt_service.generate_jwt_refresh_token(user_telegram_id)
        self.create_token(token, user_telegram_id)
        return token

    def generate_new_token_pair(self, telegram_id: int) -> TokenPair:
        self.revoke_all_refresh_tokens_of_user(telegram_id)
        return TokenPair(
            access=self._jwt_service.generate_jwt_access_token(telegram_id),
            refresh=self.generate_refresh_token(telegram_id),
        )
