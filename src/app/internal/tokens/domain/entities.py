from ninja import Schema
from ninja.orm import create_schema

from app.internal.tokens.db.models import IssuedToken

IssuedTokenOut = create_schema(IssuedToken, name="IssuedTokenOut")


class RefreshToken(Schema):
    refresh: str


class TokenPair(Schema):
    access: str
    refresh: str
