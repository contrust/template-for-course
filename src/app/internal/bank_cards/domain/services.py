import decimal
from abc import ABC, abstractmethod

from app.internal.bank_cards.domain.entities import BankCardOut


class IBankCardRepository(ABC):
    @abstractmethod
    def get_bank_card_by_number(self, bank_card_number: str) -> BankCardOut:
        pass

    @abstractmethod
    def does_bank_card_exist(self, bank_card_number: str) -> bool:
        pass

    @abstractmethod
    def does_user_have_bank_card(self, telegram_id: int, bank_card_number: str) -> bool:
        pass

    @abstractmethod
    def get_all_bank_card_numbers_of_user(self, user_telegram_id: int) -> list[str]:
        pass

    @abstractmethod
    def get_all_bank_cards_numbers_of_bank_account(self, bank_account_number: str) -> list[str]:
        pass

    @abstractmethod
    def get_bank_account_number_of_bank_card(self, bank_card_number: str) -> str:
        pass

    @abstractmethod
    def get_bank_card_balance(self, bank_card_number: str) -> decimal.Decimal:
        pass


class BankCardService:
    def __init__(self, bank_card_repository: IBankCardRepository):
        self._bank_card_repository = bank_card_repository

    def get_bank_card_by_number(self, bank_card_number: str) -> BankCardOut:
        return self._bank_card_repository.get_bank_card_by_number(bank_card_number)

    def does_bank_card_exist(self, bank_card_number: str) -> bool:
        return self._bank_card_repository.does_bank_card_exist(bank_card_number)

    def does_user_have_bank_card(self, telegram_id: int, bank_card_number: str) -> bool:
        return self._bank_card_repository.does_user_have_bank_card(telegram_id, bank_card_number)

    def get_all_bank_card_numbers_of_user(self, user_telegram_id: int) -> list[str]:
        return self._bank_card_repository.get_all_bank_card_numbers_of_user(user_telegram_id)

    def get_all_bank_cards_numbers_of_bank_account(self, bank_card_number: str) -> list[str]:
        return self._bank_card_repository.get_all_bank_cards_numbers_of_bank_account(bank_card_number)

    def get_bank_account_number_of_bank_card(self, bank_card_number: str) -> str:
        return self._bank_card_repository.get_bank_account_number_of_bank_card(bank_card_number)

    def get_bank_card_balance(self, bank_card_number: str) -> decimal.Decimal:
        return self._bank_card_repository.get_bank_card_balance(bank_card_number)
