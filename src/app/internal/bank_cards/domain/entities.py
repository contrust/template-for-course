from ninja import Schema


class BankCardOut(Schema):
    number: str
    account_number: str
