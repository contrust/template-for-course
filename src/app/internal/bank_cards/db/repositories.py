import decimal

from app.internal.bank_cards.db.models import BankCard
from app.internal.bank_cards.domain.entities import BankCardOut
from app.internal.bank_cards.domain.services import IBankCardRepository
from app.internal.exceptions.db.repositories import BankCardWithNumberNotFoundException


class BankCardRepository(IBankCardRepository):
    def get_bank_card_by_number(self, bank_card_number: str) -> BankCardOut:
        bank_card = BankCard.objects.filter(number=bank_card_number).values_list("number", "account__number").first()
        if bank_card is None:
            raise BankCardWithNumberNotFoundException(bank_card_number)
        return BankCardOut(number=bank_card[0], account_number=bank_card[1])

    def does_bank_card_exist(self, bank_card_number: str) -> bool:
        return BankCard.objects.filter(number=bank_card_number).exists()

    def does_user_have_bank_card(self, telegram_id: int, bank_card_number: str) -> bool:
        return BankCard.objects.filter(number=bank_card_number, account__owner__telegram_id=telegram_id).exists()

    def get_all_bank_card_numbers_of_user(self, user_telegram_id: int) -> list[str]:
        return list(
            BankCard.objects.filter(account__owner__telegram_id=user_telegram_id)
            .values_list("number", flat=True)
            .order_by("number")
        )

    def get_all_bank_cards_numbers_of_bank_account(self, bank_account_number: str) -> list[str]:
        return list(
            BankCard.objects.filter(account__number=bank_account_number)
            .values_list("number", flat=True)
            .order_by("number")
        )

    def get_bank_account_number_of_bank_card(self, bank_card_number: str) -> str:
        return BankCard.objects.values_list("account__number", flat=True).filter(number=bank_card_number).first()

    def get_bank_card_balance(self, bank_card_number: str) -> decimal.Decimal:
        return BankCard.objects.values_list("account__balance", flat=True).filter(number=bank_card_number).first()
