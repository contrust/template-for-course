from django.db import models

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.validators.fields.bank_card_number_validator import (
    bank_card_number_validator,
)


class BankCard(models.Model):
    number = models.CharField(unique=True, max_length=19, default="", validators=[bank_card_number_validator])
    account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.number}"
