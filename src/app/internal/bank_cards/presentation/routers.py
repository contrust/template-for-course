import decimal

from ninja import NinjaAPI, Router
from telegram.ext import (
    CommandHandler,
    ConversationHandler,
    Filters,
    MessageHandler,
    Updater,
)

from app.internal.bank_cards.domain.entities import BankCardOut
from app.internal.bank_cards.presentation.handlers import (
    BankCardAPIHandlers,
    BankCardBotHandlers,
)
from app.internal.conversation.presentation.handlers import ConversationBotHandlers
from app.internal.middlewares.auth_middlewares import HTTPJWTAuth
from app.internal.money_transactions.domain.entities import MoneyTransactionOut
from app.internal.responses.domain.entities import ErrorMessageResponse
from app.internal.responses.presentation.bot_states import READ_CARD


def get_bank_cards_api_router(bank_account_handlers: BankCardAPIHandlers, auth: HTTPJWTAuth) -> Router:
    router = Router(tags=["bank_cards"], auth=auth)

    router.add_api_operation(
        "",
        ["GET"],
        bank_account_handlers.get_all_bank_cards_numbers,
        response={200: list[str], 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/{bank_card_number}/",
        ["GET"],
        bank_account_handlers.get_bank_card_by_number,
        response={200: BankCardOut, 403: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/{bank_card_number}/balance/",
        ["GET"],
        bank_account_handlers.get_bank_card_balance,
        response={200: decimal.Decimal, 403: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/{bank_card_number}/money_transactions/",
        ["GET"],
        bank_account_handlers.get_all_money_transactions,
        response={200: list[MoneyTransactionOut], 403: ErrorMessageResponse, 404: ErrorMessageResponse},
    )

    return router


def add_bank_cards_api_router(api: NinjaAPI, bank_account_handlers: BankCardAPIHandlers, auth: HTTPJWTAuth):
    api.add_router("/bank_cards/", get_bank_cards_api_router(bank_account_handlers, auth))


def add_bank_cards_bot_handlers(
    updater: Updater, bank_cards_handlers: BankCardBotHandlers, conversation_handlers: ConversationBotHandlers
):
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("bank_card_balance", bank_cards_handlers.bank_card_ask)],
            states={
                READ_CARD: [MessageHandler(Filters.text & ~Filters.command, bank_cards_handlers.bank_card_balance)]
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
