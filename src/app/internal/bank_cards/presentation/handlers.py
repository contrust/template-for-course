import decimal

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_cards.domain.entities import BankCardOut
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bot_users.domain.services import BotUserService
from app.internal.decorators.bot_handlers_decorators import (
    require_phone_number,
    validate_bank_card_input,
    validate_bank_card_is_yours,
)
from app.internal.exceptions.presentation.handlers import ForbiddenResourceException
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.responses.presentation.bot_replies import ENTER_YOUR_BANK_CARD
from app.internal.responses.presentation.bot_states import END, READ_CARD


class BankCardAPIHandlers:
    def __init__(
        self,
        bank_card_service: BankCardService,
        bank_account_service: BankAccountService,
        money_transaction_service: MoneyTransactionService,
    ):
        self._bank_card_service = bank_card_service
        self._bank_account_service = bank_account_service
        self._money_transaction_service = money_transaction_service

    def get_all_bank_cards_numbers(self, request) -> list[str]:
        return self._bank_card_service.get_all_bank_card_numbers_of_user(request.user_id)

    def get_bank_card_by_number(self, request, bank_card_number: str) -> BankCardOut:
        if not self._bank_card_service.does_user_have_bank_card(request.user_id, bank_card_number):
            raise ForbiddenResourceException
        return self._bank_card_service.get_bank_card_by_number(bank_card_number)

    def get_bank_card_balance(self, request, bank_card_number: str) -> decimal.Decimal:
        if not self._bank_card_service.does_user_have_bank_card(request.user_id, bank_card_number):
            raise ForbiddenResourceException
        return self._bank_card_service.get_bank_card_balance(bank_card_number)

    def get_all_money_transactions(self, request, bank_card_number: str):
        bank_account_number = self._bank_card_service.get_bank_account_number_of_bank_card(bank_card_number)
        if not self._bank_account_service.does_user_have_bank_account(request.user_id, bank_account_number):
            raise ForbiddenResourceException
        return self._money_transaction_service.get_all_money_transactions_of_bank_account(bank_account_number)


class BankCardBotHandlers:
    def __init__(self, user_service: BotUserService, bank_card_service: BankCardService):
        self._user_service = user_service
        self._bank_card_service = bank_card_service

    @require_phone_number
    def bank_card_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_YOUR_BANK_CARD)
        return READ_CARD

    @require_phone_number
    @validate_bank_card_input(current_state=READ_CARD)
    @validate_bank_card_is_yours(current_state=READ_CARD)
    def bank_card_balance(self, update: Update, context: CallbackContext) -> int:
        balance = self._bank_card_service.get_bank_card_balance(update.message.text)
        update.message.reply_text(str(balance))
        return END
