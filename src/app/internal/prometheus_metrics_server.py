from prometheus_client import start_http_server

from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_accounts.presentation.metrics import (
    set_bank_account_prometheus_metrics_functions,
)
from app.internal.bot_users.db.repositories import BotUserRepository
from app.internal.bot_users.domain.services import BotUserService
from app.internal.bot_users.presentation.metrics import (
    set_bot_user_prometheus_metrics_functions,
)
from app.internal.money_transactions.db.repositories import MoneyTransactionRepository
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.money_transactions.presentation.metrics import (
    set_money_transactions_prometheus_metrics_functions,
)
from config.settings import PASSWORD_SALT


def start_metrics_server(port: int, addr: str = "") -> None:
    user_repository = BotUserRepository(PASSWORD_SALT)
    bank_account_repository = BankAccountRepository()
    money_transaction_repository = MoneyTransactionRepository()

    bot_user_service = BotUserService(user_repository)
    bank_account_service = BankAccountService(bank_account_repository)
    money_transaction_service = MoneyTransactionService(money_transaction_repository, bank_account_repository)

    set_bot_user_prometheus_metrics_functions(bot_user_service)
    set_bank_account_prometheus_metrics_functions(bank_account_service)
    set_money_transactions_prometheus_metrics_functions(money_transaction_service)

    start_http_server(port, addr)
