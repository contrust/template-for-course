from django.conf import settings
from django.core.files.storage import default_storage
from ninja import NinjaAPI

from app.internal.auth.domain.service import AuthService
from app.internal.auth.presentation.handlers import AuthAPIHandlers
from app.internal.auth.presentation.routers import add_auth_api_router
from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bank_accounts.domain.servicies import BankAccountService
from app.internal.bank_accounts.presentation.handlers import BankAccountAPIHandlers
from app.internal.bank_accounts.presentation.routers import add_bank_accounts_api_router
from app.internal.bank_cards.db.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_cards.presentation.handlers import BankCardAPIHandlers
from app.internal.bank_cards.presentation.routers import add_bank_cards_api_router
from app.internal.bot_users.db.repositories import BotUserRepository
from app.internal.bot_users.domain.services import BotUserService
from app.internal.bot_users.presentation.handlers import BotUserAPIHandlers
from app.internal.bot_users.presentation.routers import (
    add_bot_users_api_router,
    add_me_api_router,
)
from app.internal.exceptions.db.repositories import (
    InvalidArgumentException,
    NotFoundException,
)
from app.internal.exceptions.presentation.handlers import (
    ForbiddenResourceException,
    UnauthorizedException,
)
from app.internal.jwt.domain.service import JwtService
from app.internal.middlewares.auth_middlewares import HTTPJWTAuth
from app.internal.money_transactions.db.repositories import MoneyTransactionRepository
from app.internal.money_transactions.domain.services import MoneyTransactionService
from app.internal.money_transactions.presentation.handlers import (
    MoneyTransactionAPIHandlers,
)
from app.internal.money_transactions.presentation.routers import (
    add_money_transactions_api_router,
)
from app.internal.responses.domain.entities import ErrorMessageResponse
from app.internal.tokens.db.repositories import IssuedTokenRepository
from app.internal.tokens.domain.service import IssuedTokenService
from app.internal.tokens.presentation.handlers import IssuedTokenAPIHandlers
from app.internal.tokens.presentation.routers import add_tokens_api_router
from app.storages.domain.services import StorageService
from config.settings import TRANSACTION_POSTCARDS_ROOT_PATH


def get_api() -> NinjaAPI:
    api = NinjaAPI(version="1.0.0", csrf=False)

    user_repository = BotUserRepository(settings.PASSWORD_SALT)
    bank_account_repository = BankAccountRepository()
    bank_card_repository = BankCardRepository()
    token_repository = IssuedTokenRepository()
    money_transaction_repository = MoneyTransactionRepository()

    user_service = BotUserService(user_repository)
    bank_account_service = BankAccountService(bank_account_repository)
    bank_card_service = BankCardService(bank_card_repository)
    jwt_service = JwtService(
        settings.JWT_SECRET,
        settings.ACCESS_TOKEN_EXPIRATION_TIME_IN_MINUTES,
        settings.REFRESH_TOKEN_EXPIRATION_TIME_IN_MINUTES,
    )
    token_service = IssuedTokenService(token_repository, jwt_service)
    auth_service = AuthService(user_service, token_service)
    money_transaction_service = MoneyTransactionService(money_transaction_repository, bank_account_repository)
    storage_service = StorageService(default_storage)

    jwt_auth_middleware = HTTPJWTAuth(jwt_service)
    user_handlers = BotUserAPIHandlers(user_service)
    bank_card_handlers = BankCardAPIHandlers(bank_card_service, bank_account_service, money_transaction_service)
    bank_account_handlers = BankAccountAPIHandlers(bank_account_service, bank_card_service, money_transaction_service)
    money_transaction_handlers = MoneyTransactionAPIHandlers(
        money_transaction_service, bank_account_service, storage_service, TRANSACTION_POSTCARDS_ROOT_PATH
    )
    token_handlers = IssuedTokenAPIHandlers(token_service)
    auth_handlers = AuthAPIHandlers(auth_service)

    add_me_api_router(api, user_handlers, jwt_auth_middleware)
    add_bot_users_api_router(api, user_handlers, jwt_auth_middleware)
    add_money_transactions_api_router(api, money_transaction_handlers, jwt_auth_middleware)
    add_bank_accounts_api_router(api, bank_account_handlers, jwt_auth_middleware)
    add_bank_cards_api_router(api, bank_card_handlers, jwt_auth_middleware)
    add_tokens_api_router(api, token_handlers)
    add_auth_api_router(api, auth_handlers)

    return api


ninja_api = get_api()


@ninja_api.exception_handler(NotFoundException)
def object_not_found_exception_handler(request, exc: NotFoundException):
    return ninja_api.create_response(
        request,
        ErrorMessageResponse(message=f"{exc.entity_name} with {exc.field_value} {exc.entity_field} not found."),
        status=404,
    )


@ninja_api.exception_handler(InvalidArgumentException)
def invalid_argument_exception_handler(request, exc: InvalidArgumentException):
    return ninja_api.create_response(request, ErrorMessageResponse(message=exc.message), status=422)


@ninja_api.exception_handler(ForbiddenResourceException)
def forbidden_resource_exception_handler(request, exc: ForbiddenResourceException):
    return ninja_api.create_response(
        request, ErrorMessageResponse(message="You don't have permission to access this resource."), status=403
    )


@ninja_api.exception_handler(UnauthorizedException)
def unauthorized_exception_handler(request, exc: UnauthorizedException):
    return ninja_api.create_response(request, ErrorMessageResponse(message="Unauthorized."), status=401)
