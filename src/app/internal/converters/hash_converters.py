import hashlib


def convert_string_to_md5_hex_string(string: str):
    return hashlib.md5(string.encode("utf-8")).hexdigest()
