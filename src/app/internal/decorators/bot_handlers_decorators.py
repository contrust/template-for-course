import decimal
from typing import Callable

from _decimal import InvalidOperation
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.responses.presentation.bot_replies import (
    BANK_ACCOUNT_NOT_FOUND,
    BANK_ACCOUNT_NUMBER_NOT_YOURS,
    BANK_ACCOUNT_NUMBER_WRONG_FORMAT,
    BANK_CARD_NUMBER_NOT_FOUND,
    BANK_CARD_NUMBER_NOT_YOURS,
    BANK_CARD_NUMBER_WRONG_FORMAT,
    MONEY_IS_NOT_POSITIVE_NUMBER,
    NO_USER_IN_DATABASE,
    NO_YOUR_RECORD,
    PHONE_NUMBER_REQUIRED,
    PHONE_NUMBER_WRONG_FORMAT,
    TRY_AGAIN_OR_CANCEL,
    USER_NO_ACCOUNTS,
)
from app.internal.responses.presentation.bot_states import END, READ_PHONE
from app.internal.validators.fields.bank_account_number_validator import (
    does_bank_account_number_match_format,
)
from app.internal.validators.fields.bank_card_number_validator import (
    does_bank_card_number_match_format,
)
from app.internal.validators.fields.phone_number_validator import (
    does_phone_number_match_format,
)


def require_phone_number(handler: Callable) -> Callable:
    def handler_wrapper(self, update: Update, context: CallbackContext):
        telegram_id = update.effective_user.id
        if not self._user_service.does_user_with_telegram_id_exist(telegram_id):
            update.message.reply_text(NO_YOUR_RECORD)
            return END
        if not self._user_service.does_user_have_phone_number(telegram_id):
            update.message.reply_text(PHONE_NUMBER_REQUIRED)
            return END
        return handler(self, update, context)

    return handler_wrapper


def validate_bank_account_input(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            if not update.message:
                return current_state
            bank_account_number = update.message.text
            if not does_bank_account_number_match_format(bank_account_number):
                update.message.reply_text("\n".join((BANK_ACCOUNT_NUMBER_WRONG_FORMAT, TRY_AGAIN_OR_CANCEL)))
                return current_state
            if not self._bank_account_service.does_bank_account_with_number_exist(bank_account_number):
                update.message.reply_text("\n".join((BANK_ACCOUNT_NOT_FOUND, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_phone_number_input(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            if not update.message:
                return current_state
            telegram_id = update.effective_user.id
            if not self._user_service.does_user_with_telegram_id_exist(telegram_id):
                update.message.reply_text(NO_YOUR_RECORD)
                return END
            phone_number = update.message.text
            if not does_phone_number_match_format(phone_number):
                update.message.reply_text("\n".join((PHONE_NUMBER_WRONG_FORMAT, TRY_AGAIN_OR_CANCEL)))
                return READ_PHONE
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_bank_account_is_yours(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            bank_account_number = update.message.text
            telegram_id = update.effective_user.id
            if not self._bank_account_service.does_user_have_bank_account(telegram_id, bank_account_number):
                update.message.reply_text("\n".join((BANK_ACCOUNT_NUMBER_NOT_YOURS, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_bank_card_input(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            if update.message is None:
                return current_state
            bank_card_number = update.message.text
            if not does_bank_card_number_match_format(bank_card_number):
                update.message.reply_text("\n".join((BANK_CARD_NUMBER_WRONG_FORMAT, TRY_AGAIN_OR_CANCEL)))
                return current_state
            if not self._bank_card_service.does_bank_card_exist(bank_card_number):
                update.message.reply_text("\n".join((BANK_CARD_NUMBER_NOT_FOUND, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_bank_card_is_yours(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            bank_card_number = update.message.text
            telegram_id = update.effective_user.id
            if not self._bank_card_service.does_user_have_bank_card(telegram_id, bank_card_number):
                update.message.reply_text("\n".join((BANK_CARD_NUMBER_NOT_YOURS, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_username_input(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            if update.message is None:
                return current_state
            username = update.message.text
            if not self._user_service.does_user_with_username_exist(username):
                update.message.reply_text("\n".join((NO_USER_IN_DATABASE, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_user_has_bank_account(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            username = update.message.text
            if self._bank_account_service.get_user_with_username_bank_accounts_count(username) == 0:
                update.message.reply_text("\n".join((USER_NO_ACCOUNTS, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_money_input(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            if update.message is None:
                return current_state
            money_amount = update.message.text
            is_wrong_format = False
            try:
                money_amount = decimal.Decimal(money_amount)
            except InvalidOperation:
                is_wrong_format = True
            if not is_wrong_format and money_amount <= 0:
                is_wrong_format = True
            if is_wrong_format:
                update.message.reply_text("\n".join((MONEY_IS_NOT_POSITIVE_NUMBER, TRY_AGAIN_OR_CANCEL)))
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator


def validate_image_input(current_state: int) -> Callable:
    def handler_decorator(handler: Callable) -> Callable:
        def handler_wrapper(self, update: Update, context: CallbackContext):
            if update.message is None or update.message.photo is None or update.message.photo == []:
                return current_state
            return handler(self, update, context)

        return handler_wrapper

    return handler_decorator
