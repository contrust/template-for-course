from telegram import Update
from telegram.ext import CallbackContext

from app.internal.responses.presentation.bot_replies import COMMANDS


class DocsBotHandlers:
    def show_commands(self, update: Update, context: CallbackContext) -> None:
        update.message.reply_text(COMMANDS)
