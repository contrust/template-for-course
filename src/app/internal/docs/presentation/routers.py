from telegram.ext import CommandHandler, Updater

from app.internal.docs.presentation.handlers import DocsBotHandlers


def add_docs_bot_handlers(updater: Updater, docs_handlers: DocsBotHandlers):
    updater.dispatcher.add_handler(CommandHandler("help", docs_handlers.show_commands))
