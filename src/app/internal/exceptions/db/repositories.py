import decimal


class NotFoundException(Exception):
    def __init__(self, entity_name: str, entity_field_name: str, field_value):
        self.entity_name = entity_name
        self.entity_field = entity_field_name
        self.field_value = field_value


class BankAccountNotFoundException(NotFoundException):
    def __init__(self, entity_field_name: str, field_value):
        super().__init__("BankAccount", entity_field_name, field_value)


class BankAccountWithNumberNotFoundException(BankAccountNotFoundException):
    def __init__(self, bank_account_number: str):
        super().__init__("number", bank_account_number)


class BankCardNotFoundException(NotFoundException):
    def __init__(self, entity_field_name: str, field_value):
        super().__init__("BankCard", entity_field_name, field_value)


class BankCardWithNumberNotFoundException(BankCardNotFoundException):
    def __init__(self, bank_card_number: str):
        super().__init__("number", bank_card_number)


class BotUserNotFoundException(NotFoundException):
    def __init__(self, entity_field_name: str, field_value):
        super().__init__("BotUser", entity_field_name, field_value)


class BotUserWithIdNotFoundException(BotUserNotFoundException):
    def __init__(self, telegram_id: int):
        super().__init__("telegram_id", telegram_id)


class BotUserWithUsernameNotFoundException(BotUserNotFoundException):
    def __init__(self, username: str):
        super().__init__("username", username)


class InvalidArgumentException(Exception):
    def __init__(self, message: str = "Invalid argument."):
        self.message = message


class WrongAssignmentException(InvalidArgumentException):
    def __init__(self, field_name: str, field_value, message: str):
        self.field_name = field_name
        self.field_value = field_value
        super().__init__(f"Can't set {field_value} to {field_name}.\n{message}")


class NotEnoughMoneyException(InvalidArgumentException):
    def __init__(self, bank_account_number: str, money_amount: decimal.Decimal):
        self.bank_account_number = bank_account_number
        self.money_amount = money_amount
        super().__init__(f"The bank account with {bank_account_number} id doesn't have {money_amount} on the balance.")
