class ForbiddenResourceException(Exception):
    pass


class UnauthorizedException(Exception):
    pass
