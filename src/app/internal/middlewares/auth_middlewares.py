from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.jwt.domain.service import JwtService


class HTTPJWTAuth(HttpBearer):
    def __init__(self, jwt_service: JwtService):
        self._jwt_service = jwt_service
        super().__init__()

    def authenticate(self, request: HttpRequest, token):
        try:
            payload = self._jwt_service.decode_jwt_token(token)
            assert payload["type"] == "access"
            request.user_id = payload["id"]
        except Exception as e:
            print(e)
            return None
        return token
