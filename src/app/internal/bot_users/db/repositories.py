import django.core.validators
from django.contrib.auth.hashers import check_password, make_password

from app.internal.bot_users.db.models import BotUser
from app.internal.bot_users.domain.entities import BotUserIn, BotUserOut
from app.internal.bot_users.domain.services import IBotUserRepository
from app.internal.exceptions.db.repositories import (
    BotUserWithIdNotFoundException,
    BotUserWithUsernameNotFoundException,
    WrongAssignmentException,
)
from app.internal.validators.fields.password_validator import password_regex_validator
from app.internal.validators.fields.phone_number_validator import phone_regex_validator


class BotUserRepository(IBotUserRepository):
    def __init__(self, password_salt: str):
        self.password_salt = password_salt

    def _get_orm_user_by_id(self, telegram_id: int) -> BotUser:
        user = BotUser.objects.filter(telegram_id=telegram_id).first()
        if user is None:
            raise BotUserWithIdNotFoundException(telegram_id)
        return user

    def _get_bot_user_out_from_orm_user(self, orm_user: BotUser) -> BotUserOut:
        return BotUserOut(
            telegram_id=orm_user.telegram_id,
            username=orm_user.username,
            first_name=orm_user.first_name,
            last_name=orm_user.last_name,
            phone_number=(orm_user.phone_number if orm_user.phone_number is not None else ""),
        )

    def get_user_by_id(self, telegram_id: int) -> BotUserOut:
        return self._get_bot_user_out_from_orm_user(self._get_orm_user_by_id(telegram_id))

    def _get_orm_user_by_name(self, username: str) -> BotUser:
        user = BotUser.objects.filter(username=username).first()
        if user is None:
            raise BotUserWithUsernameNotFoundException(username)
        return user

    def get_user_by_name(self, username: str) -> BotUserOut:
        return self._get_bot_user_out_from_orm_user(self._get_orm_user_by_name(username))

    def does_user_with_telegram_id_exist(self, telegram_id: int) -> bool:
        return BotUser.objects.filter(telegram_id=telegram_id).exists()

    def does_user_with_username_exist(self, username: str) -> bool:
        return BotUser.objects.filter(username=username).exists()

    def does_user_have_phone_number(self, telegram_id: int) -> bool:
        try:
            phone_number = BotUser.objects.values_list("phone_number", flat=True).get(telegram_id=telegram_id)
        except BotUser.DoesNotExist:
            raise BotUserWithIdNotFoundException(telegram_id)
        return phone_number is not None

    def update_user_phone_number(self, telegram_id: int, phone_number: str) -> None:
        try:
            phone_regex_validator(phone_number)
        except django.core.validators.ValidationError as e:
            raise WrongAssignmentException("phone_number", phone_number, e.message)
        updated_count = BotUser.objects.filter(telegram_id=telegram_id).update(phone_number=phone_number)
        if updated_count == 0:
            raise BotUserWithIdNotFoundException(telegram_id)

    def get_user_password(self, telegram_id: int) -> str:
        try:
            password = BotUser.objects.values_list("password", flat=True).get(telegram_id=telegram_id)
        except BotUser.DoesNotExist:
            raise BotUserWithIdNotFoundException(telegram_id)
        return password

    def update_user_password(self, telegram_id: int, password: str) -> None:
        try:
            password_regex_validator(password)
        except django.core.validators.ValidationError as e:
            raise WrongAssignmentException("password", password, e.message)
        updated_count = BotUser.objects.filter(telegram_id=telegram_id).update(
            password=make_password(password, salt=self.password_salt)
        )
        if updated_count == 0:
            raise BotUserWithIdNotFoundException(telegram_id)

    def check_user_password(self, telegram_id: int, password: str) -> bool:
        user_password = self.get_user_password(telegram_id)
        return user_password is not None and check_password(password, user_password)

    def get_user_favorites_usernames(self, telegram_id: int) -> list[str]:
        user = self._get_orm_user_by_id(telegram_id)
        return list(user.favorites.all().values_list("username", flat=True).order_by("username"))

    def create_or_update(self, user_data: BotUserIn) -> BotUserOut:
        user, created = BotUser.objects.update_or_create(
            telegram_id=user_data.telegram_id,
            defaults={
                "username": user_data.username,
                "first_name": user_data.first_name,
                "last_name": user_data.last_name,
            },
        )
        return self._get_bot_user_out_from_orm_user(user)

    def add_favorite_user(self, telegram_id: int, favorite_user_username: str) -> None:
        user = self._get_orm_user_by_id(telegram_id)
        favorite_user = self._get_orm_user_by_name(favorite_user_username)
        user.favorites.add(favorite_user)

    def remove_favorite_user(self, telegram_id: int, favorite_user_username: str) -> None:
        user = self._get_orm_user_by_id(telegram_id)
        favorite_user = self._get_orm_user_by_name(favorite_user_username)
        user.favorites.remove(favorite_user)

    def get_bot_users_count(self) -> int:
        return BotUser.objects.count()
