from django.db import models

from app.internal.validators.fields.phone_number_validator import phone_regex_validator


class BotUser(models.Model):
    telegram_id = models.PositiveBigIntegerField(null=False, blank=False, primary_key=True)
    username = models.CharField(max_length=32, null=True, blank=True)
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    phone_number = models.CharField(validators=[phone_regex_validator], max_length=16, null=True, blank=True)
    favorites = models.ManyToManyField("self", symmetrical=False, null=True, blank=True)
    password = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return f"{self.username} {self.telegram_id}"
