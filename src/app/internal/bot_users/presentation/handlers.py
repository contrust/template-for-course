from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot_users.domain.entities import (
    BotUserIn,
    BotUserOut,
    BotUserPassword,
    BotUserPhoneNumber,
    BotUserUsername,
)
from app.internal.bot_users.domain.services import BotUserService
from app.internal.decorators.bot_handlers_decorators import (
    require_phone_number,
    validate_phone_number_input,
    validate_username_input,
)
from app.internal.responses.domain.entities import SuccessfulResponse
from app.internal.responses.presentation.bot_replies import (
    ENTER_PASSWORD,
    ENTER_PHONE_NUMBER,
    ENTER_USER,
    NO_YOUR_FAVORITES,
    NO_YOUR_RECORD,
    PASSWORD_SET,
    PASSWORD_WRONG_FORMAT,
    PHONE_NUMBER_SAVED,
    USE_HELP,
    USER_ADDED_TO_YOUR_FAVORITES,
    USER_REMOVED_FROM_YOUR_FAVORITES,
    USER_SAVED,
)
from app.internal.responses.presentation.bot_states import (
    END,
    READ_FAVORITE,
    READ_PASSWORD,
    READ_PHONE,
)
from app.internal.validators.fields.password_validator import does_password_match_format


class BotUserAPIHandlers:
    def __init__(self, bot_user_service: BotUserService):
        self._bot_user_service = bot_user_service

    def me(self, request) -> BotUserOut:
        user = self._bot_user_service.get_user_by_id(telegram_id=request.user_id)
        return BotUserOut.from_orm(user)

    def set_password(self, request, password_data: BotUserPassword) -> SuccessfulResponse:
        self._bot_user_service.update_user_password(request.user_id, password_data.password)
        return SuccessfulResponse(success=True)

    def set_phone_number(self, request, phone_number_data: BotUserPhoneNumber) -> SuccessfulResponse:
        self._bot_user_service.update_user_phone_number(request.user_id, phone_number_data.phone_number)
        return SuccessfulResponse(success=True)

    def add_favorite_user(self, request, favorite_user_data: BotUserUsername) -> SuccessfulResponse:
        self._bot_user_service.add_favorite_user(request.user_id, favorite_user_data.username)
        return SuccessfulResponse(success=True)

    def remove_favorite_user(self, request, favorite_user_data: BotUserUsername) -> SuccessfulResponse:
        self._bot_user_service.remove_favorite_user(request.user_id, favorite_user_data.username)
        return SuccessfulResponse(success=True)

    def get_favorite_users(self, request) -> list[str]:
        return self._bot_user_service.get_user_favorites_usernames(request.user_id)


class BotUserBotHandlers:
    def __init__(self, user_service: BotUserService):
        self._user_service = user_service

    def start(self, update: Update, context: CallbackContext) -> None:
        self._user_service.create_or_update(
            BotUserIn(
                telegram_id=update.effective_user.id,
                username=update.effective_user.username,
                first_name=update.effective_user.first_name,
                last_name=update.effective_user.last_name,
            )
        )

        update.message.reply_text("\n".join((USER_SAVED, USE_HELP)))

    @require_phone_number
    def me(self, update: Update, context: CallbackContext) -> None:
        user = self._user_service.get_user_by_id(update.effective_user.id)
        update.message.reply_text("\n".join((f"{key}: {value}" for key, value in user.dict().items())))

    @require_phone_number
    def show_favorites(self, update: Update, context: CallbackContext) -> None:
        favorite_users = self._user_service.get_user_favorites_usernames(update.effective_user.id)
        text = "\n".join(favorite_users) if len(favorite_users) != 0 else NO_YOUR_FAVORITES
        update.message.reply_text(text)

    @require_phone_number
    def favorite_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_USER)
        return READ_FAVORITE

    @require_phone_number
    @validate_username_input(current_state=READ_FAVORITE)
    def add_favorite_read(self, update: Update, context: CallbackContext) -> int:
        self._user_service.add_favorite_user(update.effective_user.id, update.message.text)
        update.message.reply_text(USER_ADDED_TO_YOUR_FAVORITES)
        return END

    @require_phone_number
    @validate_username_input(current_state=READ_FAVORITE)
    def remove_favorite_read(self, update: Update, context: CallbackContext) -> int:
        self._user_service.remove_favorite_user(update.effective_user.id, update.message.text)
        update.message.reply_text(USER_REMOVED_FROM_YOUR_FAVORITES)
        return END

    def set_phone_ask(self, update: Update, context: CallbackContext) -> int:
        if not self._user_service.does_user_with_telegram_id_exist(update.effective_user.id):
            update.message.reply_text(NO_YOUR_RECORD)
            return END
        update.message.reply_text(ENTER_PHONE_NUMBER)
        return READ_PHONE

    @validate_phone_number_input(current_state=READ_PHONE)
    def set_phone_read(self, update: Update, context: CallbackContext) -> int:
        self._user_service.update_user_phone_number(update.effective_user.id, update.message.text)
        update.message.reply_text(PHONE_NUMBER_SAVED)
        return END

    @require_phone_number
    def set_password_ask(self, update: Update, context: CallbackContext) -> int:
        update.message.reply_text(ENTER_PASSWORD)
        return READ_PASSWORD

    @require_phone_number
    def set_password_read(self, update: Update, context: CallbackContext) -> int:
        if update.message is None:
            return READ_PASSWORD
        password = update.message.text
        if not does_password_match_format(password):
            update.message.reply_text(PASSWORD_WRONG_FORMAT)
            return READ_PASSWORD
        self._user_service.update_user_password(update.effective_user.id, password)
        update.message.reply_text(PASSWORD_SET)
        return END
