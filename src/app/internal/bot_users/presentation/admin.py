from django.contrib import admin

from app.internal.bot_users.db.models import BotUser


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
    list_display = ("username", "first_name", "last_name", "phone_number")
