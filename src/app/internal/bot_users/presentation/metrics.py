from app.internal.bot_users.domain.services import BotUserService
from app.internal.prometheus.metrics import bot_users_count


def set_bot_user_prometheus_metrics_functions(bot_user_service: BotUserService) -> None:
    bot_users_count.set_function(bot_user_service.get_bot_users_count)
