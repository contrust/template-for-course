from ninja import NinjaAPI, Router
from telegram.ext import (
    CommandHandler,
    ConversationHandler,
    Filters,
    MessageHandler,
    Updater,
)

from app.internal.bot_users.domain.entities import BotUserOut
from app.internal.bot_users.presentation.handlers import (
    BotUserAPIHandlers,
    BotUserBotHandlers,
)
from app.internal.conversation.presentation.handlers import ConversationBotHandlers
from app.internal.middlewares.auth_middlewares import HTTPJWTAuth
from app.internal.money_transactions.domain.entities import MoneyTransactionOut
from app.internal.responses.domain.entities import (
    ErrorMessageResponse,
    SuccessfulResponse,
)
from app.internal.responses.presentation.bot_states import (
    READ_FAVORITE,
    READ_PASSWORD,
    READ_PHONE,
)


def get_me_api_router(user_handlers: BotUserAPIHandlers, auth: HTTPJWTAuth):
    router = Router(tags=["users"], auth=auth)

    router.add_api_operation("", ["GET"], user_handlers.me, response={200: BotUserOut, 404: ErrorMessageResponse})

    return router


def get_bot_users_api_router(user_handlers: BotUserAPIHandlers, auth: HTTPJWTAuth):
    router = Router(tags=["users"], auth=auth)

    router.add_api_operation(
        "/set_phone_number/",
        ["PATCH"],
        user_handlers.set_phone_number,
        response={200: SuccessfulResponse, 404: ErrorMessageResponse, 422: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/set_password/",
        ["PATCH"],
        user_handlers.set_password,
        response={200: SuccessfulResponse, 404: ErrorMessageResponse, 422: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/favorite_users/",
        ["PUT"],
        user_handlers.add_favorite_user,
        response={200: SuccessfulResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/favorite_users/",
        ["DELETE"],
        user_handlers.remove_favorite_user,
        response={200: SuccessfulResponse, 404: ErrorMessageResponse},
    )

    router.add_api_operation(
        "/favorite_users/",
        ["GET"],
        user_handlers.get_favorite_users,
        response={200: list[str], 404: ErrorMessageResponse},
    )

    return router


def add_me_api_router(api: NinjaAPI, user_handlers: BotUserAPIHandlers, auth: HTTPJWTAuth):
    api.add_router("/me/", get_me_api_router(user_handlers, auth))


def add_bot_users_api_router(api: NinjaAPI, user_handlers: BotUserAPIHandlers, auth: HTTPJWTAuth):
    api.add_router("/bot_users/", get_bot_users_api_router(user_handlers, auth))


def add_bot_users_bot_handlers(
    updater: Updater, user_handlers: BotUserBotHandlers, conversation_handlers: ConversationBotHandlers
):
    updater.dispatcher.add_handler(CommandHandler("start", user_handlers.start))
    updater.dispatcher.add_handler(CommandHandler("me", user_handlers.me))
    updater.dispatcher.add_handler(CommandHandler("favorites", user_handlers.show_favorites))
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("set_phone", user_handlers.set_phone_ask)],
            states={READ_PHONE: [MessageHandler(Filters.text & ~Filters.command, user_handlers.set_phone_read)]},
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("add_favorite", user_handlers.favorite_ask)],
            states={READ_FAVORITE: [MessageHandler(Filters.text & ~Filters.command, user_handlers.add_favorite_read)]},
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("remove_favorite", user_handlers.favorite_ask)],
            states={
                READ_FAVORITE: [MessageHandler(Filters.text & ~Filters.command, user_handlers.remove_favorite_read)]
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("set_password", user_handlers.set_password_ask)],
            states={
                READ_PASSWORD: [MessageHandler(Filters.text & ~Filters.command, user_handlers.set_password_read)],
            },
            fallbacks=[CommandHandler("cancel", conversation_handlers.cancel)],
        )
    )
