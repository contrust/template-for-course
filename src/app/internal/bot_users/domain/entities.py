from ninja import Schema


class BotUserOut(Schema):
    telegram_id: int
    username: str
    first_name: str
    last_name: str
    phone_number: str


class BotUserIn(Schema):
    telegram_id: int
    username: str
    first_name: str
    last_name: str


class BotUserPassword(Schema):
    password: str


class BotUserPhoneNumber(Schema):
    phone_number: str


class BotUserUsername(Schema):
    username: str
