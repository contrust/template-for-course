from abc import ABC, abstractmethod

from app.internal.bot_users.domain.entities import BotUserIn, BotUserOut


class IBotUserRepository(ABC):
    @abstractmethod
    def get_user_by_id(self, telegram_id: int) -> BotUserOut:
        pass

    @abstractmethod
    def get_user_by_name(self, username: str) -> BotUserOut:
        pass

    @abstractmethod
    def does_user_with_telegram_id_exist(self, telegram_id: int) -> bool:
        pass

    @abstractmethod
    def does_user_with_username_exist(self, username: str) -> bool:
        pass

    @abstractmethod
    def does_user_have_phone_number(self, telegram_id: int) -> bool:
        pass

    @abstractmethod
    def update_user_phone_number(self, telegram_id: int, phone_number: str) -> None:
        pass

    @abstractmethod
    def get_user_password(self, telegram_id: int) -> str:
        pass

    @abstractmethod
    def update_user_password(self, telegram_id: int, password: str) -> None:
        pass

    @abstractmethod
    def check_user_password(self, telegram_id: int, password: str) -> bool:
        pass

    @abstractmethod
    def get_user_favorites_usernames(self, telegram_id: int) -> list[str]:
        pass

    @abstractmethod
    def create_or_update(self, user_data: BotUserIn) -> BotUserOut:
        pass

    @abstractmethod
    def add_favorite_user(self, telegram_id: int, favorite_user_username: str) -> None:
        pass

    @abstractmethod
    def remove_favorite_user(self, telegram_id: int, favorite_user_username: str) -> None:
        pass

    @abstractmethod
    def get_bot_users_count(self) -> int:
        pass


class BotUserService:
    def __init__(self, user_repository: IBotUserRepository):
        self._user_repository = user_repository

    def get_user_by_id(self, telegram_id: int) -> BotUserOut:
        return self._user_repository.get_user_by_id(telegram_id)

    def get_user_by_name(self, username: str) -> BotUserOut:
        return self._user_repository.get_user_by_name(username)

    def does_user_with_telegram_id_exist(self, telegram_id: int) -> bool:
        return self._user_repository.does_user_with_telegram_id_exist(telegram_id)

    def does_user_with_username_exist(self, username: str) -> bool:
        return self._user_repository.does_user_with_username_exist(username)

    def does_user_have_phone_number(self, telegram_id: int) -> bool:
        return self._user_repository.does_user_have_phone_number(telegram_id)

    def update_user_phone_number(self, telegram_id: int, phone_number: str) -> None:
        return self._user_repository.update_user_phone_number(telegram_id, phone_number)

    def get_user_password(self, telegram_id: int) -> str:
        return self._user_repository.get_user_password(telegram_id)

    def update_user_password(self, telegram_id: int, password: str) -> None:
        return self._user_repository.update_user_password(telegram_id, password)

    def check_user_password(self, telegram_id: int, password: str) -> bool:
        return self._user_repository.check_user_password(telegram_id, password)

    def get_user_favorites_usernames(self, telegram_id: int) -> list[str]:
        return self._user_repository.get_user_favorites_usernames(telegram_id)

    def create_or_update(self, user_data: BotUserIn) -> BotUserOut:
        return self._user_repository.create_or_update(user_data)

    def add_favorite_user(self, telegram_id: int, favorite_user_username: str) -> None:
        return self._user_repository.add_favorite_user(telegram_id, favorite_user_username)

    def remove_favorite_user(self, telegram_id: int, favorite_user_username: str) -> None:
        return self._user_repository.remove_favorite_user(telegram_id, favorite_user_username)

    def get_bot_users_count(self) -> int:
        return self._user_repository.get_bot_users_count()
