from datetime import datetime, timedelta

import jwt


class JwtService:
    def __init__(
        self,
        jwt_secret: str,
        access_token_expiration_time_in_minutes: float,
        refresh_token_expiration_time_in_minutes: float,
    ):
        self.jwt_secret = jwt_secret
        self.access_token_expiration_time_in_minutes = access_token_expiration_time_in_minutes
        self.refresh_token_expiration_time_in_minutes = refresh_token_expiration_time_in_minutes

    def decode_jwt_token(self, jwt_token: str) -> dict:
        return jwt.decode(jwt_token, self.jwt_secret, algorithms=["HS256"])

    def generate_jwt_access_token(self, telegram_id: int):
        return jwt.encode(
            {
                "id": telegram_id,
                "type": "access",
                "exp": datetime.utcnow() + timedelta(minutes=self.access_token_expiration_time_in_minutes),
            },
            self.jwt_secret,
            algorithm="HS256",
        )

    def generate_jwt_refresh_token(self, telegram_id: int):
        return jwt.encode(
            {
                "id": telegram_id,
                "type": "refresh",
                "exp": datetime.utcnow() + timedelta(minutes=self.refresh_token_expiration_time_in_minutes),
            },
            self.jwt_secret,
            algorithm="HS256",
        )
