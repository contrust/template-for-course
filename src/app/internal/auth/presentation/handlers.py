from telegram import Update
from telegram.ext import CallbackContext

from app.internal.auth.domain.entities import IdPassword
from app.internal.auth.domain.service import AuthService
from app.internal.bot_users.domain.services import BotUserService
from app.internal.decorators.bot_handlers_decorators import require_phone_number
from app.internal.responses.presentation.bot_replies import NO_PASSWORD
from app.internal.tokens.domain.entities import TokenPair
from app.internal.tokens.domain.service import IssuedTokenService


class AuthAPIHandlers:
    def __init__(self, auth_service: AuthService):
        self._auth_service = auth_service

    def login(self, request, id_password_data: IdPassword) -> TokenPair:
        return self._auth_service.login(id_password_data.telegram_id, id_password_data.password)


class AuthBotHandlers:
    def __init__(self, user_service: BotUserService, token_service: IssuedTokenService):
        self._user_service = user_service
        self._token_service = token_service

    @require_phone_number
    def login(self, update: Update, context: CallbackContext) -> None:
        if self._user_service.get_user_password(update.effective_user.id) is None:
            update.message.reply_text(NO_PASSWORD)
            return
        token_pair = self._token_service.generate_new_token_pair(update.effective_user.id)
        update.message.reply_text(
            f"Access token :\n" f"{token_pair.access}\n" f"Refresh token :\n" f"{token_pair.refresh}"
        )
