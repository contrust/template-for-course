from ninja import NinjaAPI, Router
from telegram.ext import CommandHandler, Updater

from app.internal.auth.presentation.handlers import AuthAPIHandlers, AuthBotHandlers
from app.internal.responses.domain.entities import ErrorMessageResponse
from app.internal.tokens.domain.entities import TokenPair


def get_auth_api_router(auth_handlers: AuthAPIHandlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login/", ["POST"], auth_handlers.login, response={200: TokenPair, 404: ErrorMessageResponse}
    )

    return router


def add_auth_api_router(api: NinjaAPI, auth_handlers: AuthAPIHandlers):
    api.add_router("/", get_auth_api_router(auth_handlers))


def add_auth_bot_handlers(updater: Updater, auth_handlers: AuthBotHandlers):
    updater.dispatcher.add_handler(CommandHandler("login", auth_handlers.login))
