from ninja import Schema


class IdPassword(Schema):
    telegram_id: int
    password: str
