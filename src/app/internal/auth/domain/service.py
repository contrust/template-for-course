from app.internal.bot_users.domain.services import BotUserService
from app.internal.exceptions.presentation.handlers import UnauthorizedException
from app.internal.tokens.domain.entities import TokenPair
from app.internal.tokens.domain.service import IssuedTokenService


class AuthService:
    def __init__(self, bot_user_service: BotUserService, token_service: IssuedTokenService):
        self._bot_user_service = bot_user_service
        self._token_service = token_service

    def login(self, telegram_id: int, password: str) -> TokenPair:
        if not self._bot_user_service.check_user_password(telegram_id, password):
            raise UnauthorizedException
        return self._token_service.generate_new_token_pair(telegram_id)
