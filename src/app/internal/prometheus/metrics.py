from prometheus_client import Gauge

bot_users_count = Gauge("bot_users_count", "Total amount of bot users.")
bank_account_balance_sum = Gauge("bank_account_balance_sum", "The sum of the balances of all the bank accounts.")
money_transaction_money_amount_sum = Gauge(
    "money_transaction_money_amount_sum", "The sum of the money amounts of all the money transactions."
)
