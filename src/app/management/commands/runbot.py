from django.core.management.base import BaseCommand

from app.internal.bot import get_bot
from config.settings import TELEGRAM_BOT_WEBHOOK_URL


class Command(BaseCommand):
    def handle(self, *args, **options) -> None:
        bot = get_bot()
        bot.start_webhook(listen="0.0.0.0", port=8443, webhook_url=TELEGRAM_BOT_WEBHOOK_URL)
        bot.idle()
