from django.core.management import BaseCommand

from app.internal.prometheus_metrics_server import start_metrics_server
from config.settings import PROMETHEUS_SERVER_PORT


class Command(BaseCommand):
    def handle(self, *args, **options) -> None:
        start_metrics_server(PROMETHEUS_SERVER_PORT)
        while True:
            pass
