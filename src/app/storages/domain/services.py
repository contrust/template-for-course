from django.core.files import File
from django.core.files.storage import Storage


class StorageService:
    def __init__(self, storage: Storage):
        self.storage = storage

    def get_alternative_image_name(self, image_root_path: str) -> str:
        return self.storage.get_alternative_name(image_root_path, ".jpg")

    def create(self, path: str, content: bytearray) -> File:
        file = self.storage.open(path, "w")
        file.write(content)
        file.close()
        return file
