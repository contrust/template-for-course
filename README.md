# Bank app
A bank application written in Django. 
It can be run as a Telegram bot or as a server.
# Environmental variables
In order to run a bot or a server, you have to create .env file with all the environmental variables from the .env.example file. 
## Docs
[Environmental variables](docs/environmental_variables.md)
# Telegram bot
## Run
```sh
make run_bot
```
## Docs
[Bot commands](docs/bot_commands.md)

# Server
## Run
```sh
make run_server
```
## Endpoints
[Server endpoints](docs/server_endpoints.md)